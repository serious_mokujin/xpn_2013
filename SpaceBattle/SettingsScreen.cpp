#include "SettingsScreen.h"
#include"ScreenManager.h"

SettingsScreen::SettingsScreen(ScreenManager* manager) : Screen(manager)
{
}
void SettingsScreen::Load()
{
	this->backTex = Game::getResMgr()->getTexture("background");
	
	font = Game::getResMgr()->getFont("space");
	
	ISoundSource* s1 = Game::getResMgr()->getSound("select");
	ISoundSource* s2 = Game::getResMgr()->getSound("choose");
	_titleItem = new TextItem("SETTINGS", Game::getResMgr()->getFont("spaceBig"), SColor(255, 255, 215, 0), vector2df(0, -2000),SCREEN_WIDTH, 60); 
	_fpsItem = new MenuItem(Helper::format(L"FPS counter: %s",(Game::getSettings()->fpsVisible ? L"On" : L"Off")),
		font, MENU_ITEM_COLOR, MENU_HOVER_COLOR, MENU_SELECT_COLOR, s1, s2,vector2df(0, -300), SCREEN_WIDTH, 60); 
	_soundItem = new MenuItem(Helper::format(L"Sounds: %s",(Game::getSettings()->getSoundsState() ? L"On" : L"Off")), 
		font, MENU_ITEM_COLOR, MENU_HOVER_COLOR, MENU_SELECT_COLOR, s1, s2,vector2df(0, -360), SCREEN_WIDTH, 60);
	_musicItem = new MenuItem(Helper::format(L"Music: %s",(Game::getSettings()->getMusicState() ? L"On" : L"Off")),
		font, MENU_ITEM_COLOR, MENU_HOVER_COLOR, MENU_SELECT_COLOR, s1, s2,vector2df(0, -420), SCREEN_WIDTH, 60);
	_soundVolumeItem = new MenuItem(Helper::format(L"Sound volume: %d", (int)(Game::getSettings()->getSoundVolume()*100.0)), 
		font, MENU_ITEM_COLOR, MENU_HOVER_COLOR ,MENU_SELECT_COLOR, s1, s2,vector2df(0, -480), SCREEN_WIDTH, 60);
	_musicVolumeItem = new MenuItem(Helper::format(L"Music volume: %d", (int)(Game::getSettings()->getMusicVolume()*100.0)), 
		font, MENU_ITEM_COLOR, MENU_HOVER_COLOR ,MENU_SELECT_COLOR, s1, s2,vector2df(0, -540), SCREEN_WIDTH, 60);
	_backItem =  new MenuItem("back", font, MENU_ITEM_COLOR, MENU_HOVER_COLOR ,MENU_SELECT_COLOR, s1, s2, vector2df(0, SCREEN_HEIGHT+100), SCREEN_WIDTH, 40);
	items->push_back(_titleItem);
	items->push_back(_fpsItem);
	items->push_back(_soundItem);
	items->push_back(_musicItem);
	items->push_back(_soundVolumeItem);
	items->push_back(_musicVolumeItem);
	items->push_back(_backItem);
	_fpsItem->Link(_backItem, _soundItem);
	_soundItem->Link(_fpsItem, _musicItem);
	_musicItem->Link(_soundItem, _soundVolumeItem);
	_soundVolumeItem->Link(_musicItem, _musicVolumeItem);
	_musicVolumeItem->Link(_soundVolumeItem, _backItem);
	_backItem->Link(_musicVolumeItem, _fpsItem);

	_titleItem->LerpTo(0, 100, 3);
	_fpsItem->LerpTo(0, 300, 3);
	_soundItem->LerpTo(0, 360, 3);
	_musicItem->LerpTo(0, 420, 3);
	_soundVolumeItem->LerpTo(0, 480, 3);
	_musicVolumeItem->LerpTo(0, 540, 3);
	_backItem->LerpTo(0, SCREEN_HEIGHT - 60, 4);
}
void SettingsScreen::Unload(){}

void SettingsScreen::Update(irr::f32 dt)
{
	Screen::Update(dt);
	if(_fpsItem->IsPressed())
	{
		Game::getSettings()->fpsVisible = !Game::getSettings()->fpsVisible;
		_fpsItem->setText(Helper::format(L"FPS counter: %s",(Game::getSettings()->fpsVisible ? L"On" : L"Off")));
		_fpsItem->Release();
	}
	else if(_soundItem->IsPressed())
	{
		Game::getSettings()->setSoundsState(!Game::getSettings()->getSoundsState());
		_soundItem->setText(Helper::format(L"Sounds: %s",(Game::getSettings()->getSoundsState() ? L"On" : L"Off")));
		_soundItem->Release();
	}
	else if(_musicItem->IsPressed())
	{
		Game::getSettings()->setMusicState(!Game::getSettings()->getMusicState());
		_musicItem->setText(Helper::format(L"Music: %s",(Game::getSettings()->getMusicState() ? L"On" : L"Off")));
		_musicItem->Release();
	}
	else if(_soundVolumeItem->IsPressed())
	{
		Game::getSettings()->setSoundVolume((Game::getSettings()->getSoundVolume()+0.1f));
		_soundVolumeItem->setText(Helper::format(L"Sound volume: %d", (int)(Game::getSettings()->getSoundVolume()*100.0)));
		_soundVolumeItem->Release();
	}
	else if(_musicVolumeItem->IsPressed())
	{
		Game::getSettings()->setMusicVolume((Game::getSettings()->getMusicVolume()+0.1f));
		_musicVolumeItem->setText(Helper::format(L"Music volume: %d", (int)(Game::getSettings()->getMusicVolume()*100.0)));
		_musicVolumeItem->Release();
	}
	else if(_backItem->IsPressed() || InputReceiver::IsKeyDown(KEY_ESCAPE))
		changeScreen(new MainMenuScreen(manager));
}
void SettingsScreen::Draw()
{
	Screen::Draw();
	Screen::DrawInterface();
}

SettingsScreen::~SettingsScreen(void)
{
}
