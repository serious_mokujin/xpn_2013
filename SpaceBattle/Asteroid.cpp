#include "Asteroid.h"

std::list<Asteroid*>  Asteroid::pool;

Asteroid::Asteroid(void):GameObject("asteroid")
{
	_node->setAnimationSpeed(0);
}
void Asteroid::Update(f32 dt)
{
	vector3df p = _node->getPosition();
	p.X += dt*_speed.X;
	p.Y += dt*_speed.Y;
	p.Z += dt*_speed.Z; //lerp<f32>(p.Z, -50, dt/7);
	_node->setPosition(p);
	vector3df r = _node->getRotation();
	r.Z += dt;
	r.Y += dt;
	_node->setRotation(r);

	if(p.Y < BOTTOM_DEATH_LINE)
	{
		_active = false;
		_node->setVisible(false);
	}
}

void Asteroid::UpdatePool(f32 dt)
{
	for(std::list<Asteroid*>::iterator i = pool.begin(); i != pool.end(); ++i)
		(*i)->Update(dt);
}
void Asteroid::ClearPool()
{
	for(std::list<Asteroid*>::iterator i = pool.begin(); i != pool.end(); ++i)
		delete (*i);
	pool.clear();
}
void Asteroid::LaunchAsteroid()
{
	for(std::list<Asteroid*>::iterator i = pool.begin(); i!= pool.end(); ++i)
		if(!(*i)->_active)
		{
			(*i)->Launch();
			return;
		}
	Asteroid* r = new Asteroid();
	pool.push_back(r);
	r->Launch();
}

void Asteroid::Launch()
{
	_node->setPosition(vector3df(rand()%1000-500, 320, 100+rand()%1000));
	_node->setScale(vector3df((1+rand()%30)/15.0f));
	_node->setRotation(vector3df(rand()%360, rand()%360,rand()%360));
	_speed.X = ((float)(rand()%80))/5.0f;
	_speed.Y = -((float)(rand()%100+100))/10.0f;
	_speed.Z = 0;
	_active = true;
	_node->setVisible(true);
}

Asteroid::~Asteroid(void)
{
}
