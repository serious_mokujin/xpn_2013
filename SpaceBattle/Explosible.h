#pragma once
#include<irrlicht.h>
#include"Game.h"
#include "ParticleEngine/IParticleSystem.h"
#include "ParticleEngine/IParticleDrawer.h"
#include "ParticleEngine/IEmitterRegion.h"
#include "ParticleEngine/IParticleEmitter.h"
#include "ParticleEngine/Particle.h"
#include "Affectors.h"

using namespace irr;
using namespace irr::video;
using namespace irr::core;
using namespace irr::scene;


class Explosible
{

public:
	void addExplosion(const vector3df& pos, const u32& scale, ISceneNode* parent );

};

