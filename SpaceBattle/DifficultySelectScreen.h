#pragma once
#include "Screen.h"
#include "MenuItem.h"
#include "ISceneManager.h"
#include <irrlicht.h>
#include "MainMenuScreen.h"

using namespace irr;
using namespace irr::core;
using namespace irr::gui;

class DifficultySelectScreen :
	public Screen
{
private:
	virtual void Draw();
	IGUIFont* font;
	IGUIFont* fontBig;
	TextItem* _titleItem;
	MenuItem* _easyItem;
	MenuItem* _middleItem;
	MenuItem* _hardItem;
	MenuItem* _crazyItem;
	MenuItem* _backItem;
public:
	DifficultySelectScreen(ScreenManager* manager);
	virtual void Load();
	virtual void Unload();
	virtual void Update(irr::f32 dt);
	virtual ~DifficultySelectScreen(void);
};

