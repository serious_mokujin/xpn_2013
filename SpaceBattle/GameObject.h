#pragma once
#include<irrlicht.h>
#include"ResourceManager.h"
#include<string>
#include"Game.h"
using namespace irr;
using namespace irr::scene;
using namespace std;

class GameObject
{
protected:
	IAnimatedMeshSceneNode* _node;
private:

public:
	GameObject(std::string name);
	virtual void Update(irr::f32 dt) = 0;
	aabbox3df GetHitbox(){return _node->getTransformedBoundingBox(); }
	void setPosition(f32 x, f32 y, f32 z);
	void setPosition(vector3df pos);
	vector3df getPosition();
	
	virtual ~GameObject(void);
};

