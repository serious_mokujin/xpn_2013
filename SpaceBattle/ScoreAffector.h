#pragma once
class ScoreContainer;
class ScoreAffector
{
private:
	ScoreContainer* container;
public:
	ScoreAffector(ScoreContainer* container);
	void AffectScore(int value);
};

