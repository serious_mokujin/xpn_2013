#include "AproveScreen.h"


AproveScreen::AproveScreen(stringw title, Screen* yes, Screen* no, ScreenManager* manager) : Screen(manager)
{
	this->_title = title;
	this->_yesScreen = yes;
	this->_noScreen = no;
}
void AproveScreen::Load()
{
	this->backTex = Game::getResMgr()->getTexture("background");
	font = Game::getResMgr()->getFont("space");
	_titleItem = new TextItem(_title, font, MENU_ITEM_COLOR, vector2df(0, -60), SCREEN_WIDTH, 60);
	ISoundSource* s1 = Game::getResMgr()->getSound("select");
	ISoundSource* s2 = Game::getResMgr()->getSound("choose");
	_yesItem = new MenuItem("yes", font, MENU_ITEM_COLOR, MENU_HOVER_COLOR, MENU_SELECT_COLOR, s1, s2,vector2df(-SCREEN_WIDTH/2, 480), SCREEN_WIDTH/2, 60);
	_noItem = new MenuItem("no", font, MENU_ITEM_COLOR, MENU_HOVER_COLOR ,MENU_SELECT_COLOR, s1, s2,vector2df(SCREEN_WIDTH, 480), SCREEN_WIDTH/2, 60);
	items->push_back(_titleItem);
	items->push_back(_yesItem);
	items->push_back(_noItem);
	_yesItem->Link(_noItem, _noItem);
	_noItem->Link(_yesItem, _yesItem);
	_titleItem->LerpTo(0, 320, 4);
	_yesItem->LerpTo(0, 480, 3);
	_noItem->LerpTo(SCREEN_WIDTH/2, 480, 3);
}
void AproveScreen::Unload()
{

}
void AproveScreen::Update(irr::f32 dt)
{
	Screen::Update(dt);
	if(_yesItem->IsPressed()) changeScreen(_yesScreen);
	else if(_noItem->IsPressed()) changeScreen(_noScreen);
}
void AproveScreen::Draw()
{
	Screen::Draw();
	Screen::DrawInterface();
}
AproveScreen::~AproveScreen(void)
{
}
