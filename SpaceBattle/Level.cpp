#include "Level.h"

Level::Level()
{	
	_enemies = new vector<Enemy*>();
	Rebuild();
	_pushDown = 20;
}
void Level::Rebuild()
{

	for (auto i = _enemies->begin(); i != _enemies->end(); ++i)
		delete (*i);
	_enemies->clear();
	_width = 5;
	_height = Game::getSettings()->difficulty;
	_enemiesSpeed = 30;

	for (int j = 0; j < _height; j++)
	{
		for (int i = 0; i < _width; i++)
		{
			int e = rand()%4;
			std::string name = e == 0 ? "enemy1" : e == 1?"enemy2" : e == 2 ? "enemy3" : "enemy4";
			_enemies->push_back(new Enemy(&score, name, i-_width/2, j + 2)); 
		}
	}
	_newDirection = _moveDirection = true;
	score.setScore(0);
	this->_game = true;
}
void Level::setShip(Ship* ship) { _ship=ship; }
Ship* Level::getShip() const { return _ship; }
bool Level::IsOver() const { return !this->_game; }
bool Level::IsWin() const { return this->_win; }
bool Level::IsAction() const { return _action; }
int Level::getScore() {return score.getScore();}

void Level::Update(f32 dt)
{
	_action = false;
	UpdateEnemies(dt);
	
	if(_ship)
		_ship->Update(dt);
}
void Level::MoveEnemies(f32 dt)
{
	if(_moveDirection != _newDirection)
	{
		f32 d = dt*_enemiesSpeed;
		_pushDown -= dt*_enemiesSpeed;
		if(_pushDown < 0)
		{
			d = dt*_enemiesSpeed+_pushDown;
			_moveDirection = _newDirection;
			_pushDown = 20;
		}
		for (auto i = _enemies->begin(); i != _enemies->end(); ++i)
			if((*i)->Down(d))
				gameOver(false);
	}
	else
	{
		for (auto i = _enemies->begin(); i != _enemies->end(); ++i)
		{
			if((*i)->Move(_moveDirection, dt*_enemiesSpeed))
				_newDirection = !_moveDirection;
		}
	}
}
void Level::UpdateEnemies(f32 dt)
{
	if(!_game)
	{
		if(!_win)
			for (auto i = _enemies->begin(); i != _enemies->end(); ++i)
				(*i)->Down(dt*50);
	}
	else
	{
		MoveEnemies(dt);
	}

	bool enemiesLeft = false;
	for (auto i = _enemies->begin(); i != _enemies->end(); ++i)
	{
		bool oldDestroyed = (*i)->IsDestroyed();
		bool oldDamaged = (*i)->IsDamaged();
		(*i)->Update(dt);
		if((!oldDestroyed && (*i)->IsDestroyed()) || (!oldDamaged && (*i)->IsDamaged()))
		{
			_action = true;
			_enemiesSpeed += 0.75f;
		}
		if(!(*i)->IsDestroyed())
			enemiesLeft = true;
	}
	if(!enemiesLeft)
	{
		gameOver(true);
		_ship->FlyAway();
	}
	
}

void Level::gameOver(bool win)
{
	if(!_game) return;
	if(!win) _ship->Destroy();
	AudioManager::PlaySound(Game::getResMgr()->getSound(win ? "win" : "lose"));
	_game = false;
	_win = win;
}
Level::~Level(void)
{
	for (auto i = _enemies->begin(); i != _enemies->end(); ++i)
		delete (*i);
	delete _enemies;	
}
