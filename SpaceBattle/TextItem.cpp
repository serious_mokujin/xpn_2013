#include "TextItem.h"
#include "Screen.h"

TextItem::TextItem(stringw text, IGUIFont* font, SColor color, vector2df pos, f32 width, f32 height)
{
	this->text = text;
	this->font = font;
	this->color = color;
	this->pos = pos;
	this->_next = pos;
	this->width = width;
	this->height = height;
	this->_speed = 1;
}
recti TextItem::getRect()
{
	return recti((s32)pos.X, (s32)pos.Y, pos.X + width, pos.Y+height);
}
void TextItem::LerpTo(vector2df pos, f32 speed) { _next = pos; this->_speed = speed; }
void TextItem::LerpTo(f32 x, f32 y, f32 speed) { _next = vector2df(x, y); this->_speed = speed; }
vector2df TextItem::getPosition() { return pos; }
void TextItem::setPosition(vector2df pos) { pos = pos; _next = pos; }
void TextItem::setPosition(f32 x, f32 y) { pos = vector2df(x, y); _next = vector2df(x, y); }
void TextItem::Draw()
{
	font->draw(text, getRect(), color, true, true);
}
stringw TextItem::getText() { return text; }
void TextItem::setText(stringw text) { this->text = text; }
void TextItem::setColor(SColor color) { this->color = color; }
SColor TextItem::getColor() { return this->color; }
void TextItem::Update(f32 dt)
{
	pos.X = lerp<f32>(pos.X, _next.X, dt*_speed);
	pos.Y = lerp<f32>(pos.Y, _next.Y, dt*_speed);
}

TextItem::~TextItem(void)
{
}
