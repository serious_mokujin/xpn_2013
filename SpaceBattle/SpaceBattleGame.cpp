#include "SpaceBattleGame.h"
#include"LoadScreen.h"

SpaceBattleGame::SpaceBattleGame(void):Game()
{
}
void SpaceBattleGame::Load()
{
	_fpsLabel = gui->addStaticText(L"FPS: ", core::rect<s32>(20, 20, 300, 60), false);
	_fpsLabel->setBackgroundColor(video::SColor(0, 255, 255, 255));
	_fpsLabel->setOverrideColor(video::SColor(255, 255,255,255));
	
	rcmgr->loadFont("space", "content/fonts/spaceFont.xml");
	rcmgr->loadFont("spaceBig", "content/fonts/space36.xml");

	rcmgr->loadSound("select", "content/sounds/menu_move.ogg", true);
	rcmgr->loadSound("choose", "content/sounds/menu_select.ogg", true);
	rcmgr->loadSound("menuTheme", "content/music/space.ogg", false);
	rcmgr->loadSound("gameTheme", "content/music/space2.ogg", false);

	_fpsLabel->setOverrideFont(rcmgr->getFont("space"));

	rcmgr->loadTexture("background","content/textures/background.png"); 
	rcmgr->loadTexture("title","content/textures/title.png"); 
	rcmgr->loadTexture("load","content/textures/load.png");
	rcmgr->loadTexture("exit","content/textures/exit.png");
	rcmgr->loadTexture("about","content/textures/about.png");
	rcmgr->loadTexture("help","content/textures/help.png");

	_screenManager = new ScreenManager();
	_screenManager->setScreen(new SplashScreen(_screenManager));
}
void SpaceBattleGame::Update(f32 dt)
{
	_fpsLabel->setVisible(settings->fpsVisible);
	if(settings->fpsVisible)
		if(_fps != driver->getFPS())
		{
			_fps = driver->getFPS();
			_fpsLabel->setText(Helper::format(L"FPS: %d", _fps).c_str());
		}
	_screenManager->update(dt);
		
	driver->beginScene(true, true, irr::video::SColor(255, 255, 255, 255));
	_screenManager->draw();
	gui->drawAll();
	
	driver->endScene();
}
void SpaceBattleGame::Unload()
{
	_screenManager->clearScreens();
	delete _screenManager;
}
SpaceBattleGame::~SpaceBattleGame(void)
{
}
