#pragma once
#include "screen.h"
#include<irrlicht.h>
using namespace irr;
using namespace irr::video;

class SplashScreen :
	public Screen
{
private:
	f32 _timeLeft;
public:
	SplashScreen(ScreenManager* manager);
	virtual void Load();
	virtual void Unload();
	virtual void Update(irr::f32 dt);
	virtual ~SplashScreen(void);
};

