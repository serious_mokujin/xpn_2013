#pragma once
#include "gameobject.h"
#include<irrlicht.h>
#include"ResourceManager.h"
#include<string>
#include"GlobalConst.h"
#include"Rocket.h"
#include"Helper.h"
#include"ScoreAffector.h"
#include"Explosible.h"

using namespace irr;
using namespace irr::scene;
using namespace irr::video;
using namespace std;

#define ENEMY_SIZE 60

class Enemy :
	public GameObject, ScoreAffector, Explosible
{
private:
	int _x, _y;
	int _life;
	f32 _fireTimeout;
	bool _destroyed;
	bool _damaged;
	vector3df _flyDir;
	vector3df _accel;
	vector3df _rotation;
	float _speed;
public:
	Enemy(ScoreContainer* score, std::string name, int x, int y);
	virtual void Update(f32 dt);
	bool IsDestroyed();
	bool IsDamaged();
	bool Move(bool direction, f32 displacement); // ���������� true ���� ������������ �� ������� ���������
	bool Down(f32 displacement);				// ���������� true ���� ������ ���������
	virtual ~Enemy(void);
};

