#include "ExitSplashScreen.h"


ExitSplashScreen::ExitSplashScreen(ScreenManager* manager):Screen(manager)
{
	_timeLeft = 3;
}

void ExitSplashScreen::Load()
{
	this->backTex = Game::getResMgr()->getTexture("exit");
}
void ExitSplashScreen::Update(irr::f32 dt)
{
	f32 v = Game::getSoundEngine()->getSoundVolume();
	if(v > 0)
	{
		v -= dt;
		Game::getSoundEngine()->setSoundVolume(max(v, 0.0f));
		if(v <= 0)
			Game::getSoundEngine()->stopAllSounds();
	}

	if(_timeLeft > 0)
		_timeLeft -= dt;
	else 
		Game::getDevice()->closeDevice();
}

ExitSplashScreen::~ExitSplashScreen(void)
{
}
