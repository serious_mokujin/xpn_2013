#include "LoadScreen.h"
#include "Game.h"
#include "GameScreen.h"
#include"ScreenManager.h"

LoadScreen::LoadScreen(ScreenManager* manager):Screen(manager)
{
	_time = 1.0f;
	_loaded = false;
}


void LoadScreen::Load()
{
	this->backTex = Game::getResMgr()->getTexture("load");

}
void LoadScreen::Unload()
{
	Game::getSoundEngine()->setSoundVolume(1.0f);
}
void LoadScreen::Update(irr::f32 dt)
{
	f32 v = Game::getSoundEngine()->getSoundVolume();
	if(v > 0)
	{
		v -= dt;
		Game::getSoundEngine()->setSoundVolume(max(v, 0.0f));
		if(v <= 0)
			Game::getSoundEngine()->stopAllSounds();
	}

	_time -= dt;
	if(_time <= 0)
	{
		if(!_loaded)
		{
			Game::getResMgr()->loadMesh("enemy1", "content/models/enemies_01.X");
			Game::getResMgr()->loadTexture("enemy1", "content/textures/enemies_01.png");

			Game::getResMgr()->loadMesh("enemy2", "content/models/enemies_02.X");
			Game::getResMgr()->loadTexture("enemy2", "content/textures/enemies_02.png");

			Game::getResMgr()->loadMesh("enemy3", "content/models/enemies_03.X");
			Game::getResMgr()->loadTexture("enemy3", "content/textures/enemies_03.png");

			Game::getResMgr()->loadMesh("enemy4", "content/models/enemies_04.X");
			Game::getResMgr()->loadTexture("enemy4", "content/textures/enemies_04.png");

			Game::getResMgr()->loadMesh("ship", "content/models/fighter.X");
			Game::getResMgr()->loadTexture("ship", "content/textures/fighter.png");

			Game::getResMgr()->loadMesh("rocket", "content/models/rocket.X");
			Game::getResMgr()->loadTexture("rocket", "content/textures/rocket.png");

			Game::getResMgr()->loadMesh("asteroid", "content/models/asteroid.X");
			Game::getResMgr()->loadTexture("asteroid", "content/textures/asteroid.png");
			Game::getResMgr()->loadTexture("particles","content/textures/particle_atlas_additive.png");

			Game::getResMgr()->loadSound("fire", "content/sounds/rocket_launch.ogg", true);
			Game::getResMgr()->loadSound("explosion1", "content/sounds/explosion1.ogg", true);
			Game::getResMgr()->loadSound("explosion2", "content/sounds/explosion2.ogg", true);
			Game::getResMgr()->loadSound("win", "content/sounds/win.ogg", true);
			Game::getResMgr()->loadSound("lose", "content/sounds/lose.ogg", true);

			_loaded = true;
			_time = 1.0f;
		}
		else
		{
			changeScreen(new GameScreen(manager));
		}
	}
}

LoadScreen::~LoadScreen(void)
{
}
