#pragma once
#include "Screen.h"
#include"MenuItem.h"
#include<string>

class AproveScreen :
	public Screen
{
private:
	virtual void Draw();
	IGUIFont* font;
	TextItem* _titleItem;
	MenuItem* _yesItem;
	MenuItem* _noItem;
	stringw _title;
	Screen* _yesScreen;
	Screen* _noScreen;
public:
	AproveScreen(stringw title, Screen* yes, Screen* no, ScreenManager* manager);
	virtual void Load();
	virtual void Unload();
	virtual void Update(irr::f32 dt);
	virtual ~AproveScreen(void);
};

