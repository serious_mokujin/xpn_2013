#pragma once
class ScoreAffector;
class ScoreContainer
{
friend class ScoreAffector;
private:
	int score;
public:
	ScoreContainer(void);
	int getScore();
	void setScore(int score);
};

