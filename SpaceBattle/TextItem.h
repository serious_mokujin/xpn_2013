#pragma once
#include<string>
#include<irrlicht.h>
#include"Game.h"

using namespace std;
using namespace irr;
using namespace irr::gui;
using namespace irr::core;
using namespace irr::video;

class Screen;
class TextItem
{
protected:
	IGUIFont* font;
	stringw text;
	SColor color;
	f32 width, height;
	vector2df pos;
	recti getRect();
private:
	vector2df _next;
	f32 _speed;
public:
	TextItem(stringw text, IGUIFont* font, SColor color, vector2df pos, f32 width, f32 height);
	void LerpTo(vector2df pos, f32 speed = 1.0f);
	void LerpTo(f32 x, f32 y, f32 speed = 1.0f);
	virtual void Draw();
	
	stringw getText();
	void setText(stringw text);

	void setColor(SColor color);
	SColor getColor();
	
	vector2df getPosition();
	void setPosition(vector2df pos);
	void setPosition(f32 x, f32 y);

	virtual void Update(f32 dt);
	virtual ~TextItem(void);
};

