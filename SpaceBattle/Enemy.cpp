#include "Enemy.h"


Enemy::Enemy(ScoreContainer* score, std::string name, int x, int y) : GameObject(name), ScoreAffector(score)
{
	this->_x = x;
	this->_y = y;
	if(name == "enemy4" || name == "enemy3")
		this->_life = 2;
	else
		this->_life = 1;
	_node->setPosition(core::vector3df(_x*ENEMY_SIZE , _y*ENEMY_SIZE, 0));
	_node->setCurrentFrame(rand()%600);
	_destroyed = false;
	_damaged = false;
	_accel = vector3df(0, -100, 0);
	_rotation = vector3df(((rand()%20)/10.0f),((rand()%20)/10.0f), ((rand()%20)/10.0f));
	int c = rand()%3;
	if(c == 0) _rotation.X = 0;
	else if(c == 1) _rotation.Y = 0;
	else _rotation.Z = 0;
}
bool Enemy::IsDestroyed() { return _destroyed; }
bool Enemy::IsDamaged() { return _damaged; }
void Enemy::Update(f32 dt)
{
	if(_node)
	{
		if(_destroyed || _damaged)
		{
			_speed -= (_damaged ? dt*5 : dt);
			vector3df r = _node->getRotation();
			r += _rotation*_speed;
			_node->setRotation(r);
			if(_speed <= 1) _damaged = false;
		}
		if(_destroyed)
		{
			_flyDir.X += _accel.X*dt;
			_flyDir.Y += _accel.Y*dt;
			_flyDir.Z += _accel.Z*dt;
		
			vector3df p = _node->getPosition();
			p += _flyDir*dt;
			_node->setPosition(p);
		}
		else
		{
			if(!_damaged)
			{
				vector3df rotation = _node->getRotation();
				if(abs(rotation.X)>5)
					rotation.X =  lerp<double>(rotation.X, (double)(((int)rotation.X/360)*360), dt*5);
				if(abs(rotation.Y)>5)
					rotation.Y =  lerp<double>(rotation.Y, (double)(((int)rotation.Y/360)*360), dt*5);
				if(abs(rotation.Z)>5)
					rotation.Z =  lerp<double>(rotation.Z, (double)(((int)rotation.Z/360)*360), dt*5);
				_node->setRotation(rotation);
			}
			Rocket* r = Rocket::CheckCollisions(this);
			if(r)
			{
				r->Frezee();
				vector3df v;
				v.Y = 80;
				v.Z = -150;
				v.X = rand()%20 -10;
				_speed = 10.0f;
				_flyDir = v;
				AffectScore(5);
				_damaged = true;
				if(--_life == 0)
				{
					_destroyed = true;
					AffectScore(10);
				}
				addExplosion(vector3df(0, 0, -15), 8, _node);
			
				AudioManager::PlaySound(Game::getResMgr()->getSound(rand()%2 ? "explosion1" : "explosion2"));
			}
		}
		if(_node->getPosition().Y < BOTTOM_DEATH_LINE)
		{
			_node->remove();
			_node = NULL;
		}
	}
}

bool Enemy::Move(bool direction, f32 displacement)
{
	if(_destroyed) return false;
	core::vector3df p = _node->getPosition();
	if(direction) // right
	{
		p.X += (_y % 2) ? displacement*2 : displacement;
		if(p.X > LEVEL_RIGHT_SIDE)
			p.X = LEVEL_RIGHT_SIDE;
	}
	else // left
	{
		p.X -= (_y % 2) ? displacement*2 : displacement;
		if(p.X < LEVEL_LEFT_SIDE)
			p.X = LEVEL_LEFT_SIDE;
	}
	
	_node->setPosition(p);
	
	return (p.X <= LEVEL_LEFT_SIDE || p.X >= LEVEL_RIGHT_SIDE);
}
bool Enemy::Down(f32 displacement)
{
	if(_destroyed) return false;
	core::vector3df p = _node->getPosition();
	p.Y -= displacement;
	_node->setPosition(p);
	if(p.Y < BOTTOM_DEATH_LINE)
	{	
		_destroyed = true;
		_node->remove();
		_node = NULL;
		return true;
	}
	if(p.Y < -100)
	{
		return true;
	}
	return false;
}

Enemy::~Enemy(void)
{
}
