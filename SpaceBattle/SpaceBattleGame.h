#pragma once
#include "Game.h"
#include "Level.h"
#include"ScreenManager.h"
#include"SplashScreen.h"
#include "ParticleEngine/IParticleSystem.h"
#include "ParticleEngine/IParticleDrawer.h"
#include "ParticleEngine/IEmitterRegion.h"
#include "ParticleEngine/IParticleEmitter.h"
#include "ParticleEngine/Particle.h"

using namespace irr;
using namespace irr::video;
using namespace irr::core;
using namespace irr::scene::particle;

class SpaceBattleGame : public Game
{
private:
	ScreenManager* _screenManager;
	IGUIStaticText* _fpsLabel;
	int _fps;
public:
	SpaceBattleGame(void);
	virtual void Load();
	virtual void Update(f32 dt);
	virtual void Unload();
	void addExplosion(const vector3df& pos, const u32& scale);
	~SpaceBattleGame(void);
};

