#include "GameObject.h"


GameObject::GameObject(std::string name)
{
	IAnimatedMesh* m = Game::getResMgr()->getMesh(name);
	_node = Game::getScene()->addAnimatedMeshSceneNode(m);
	if(_node)
	{
		_node->setMaterialTexture(0, Game::getResMgr()->getTexture(name));
		_node->setMaterialFlag(video::E_MATERIAL_FLAG::EMF_LIGHTING, true);
		_node->setAnimationSpeed(2000);
		
	}
}
void GameObject::setPosition(f32 x, f32 y, f32 z)
{
	_node->setPosition(vector3df(x, y, z));
	_node->updateAbsolutePosition();
}
void GameObject::setPosition(vector3df pos)
{
	_node->setPosition(pos);
	_node->updateAbsolutePosition();
}
vector3df GameObject::getPosition()
{
	return _node->getPosition();
}


GameObject::~GameObject(void)
{
	if(_node)
		_node->remove();
}
