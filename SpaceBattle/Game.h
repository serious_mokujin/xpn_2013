#pragma once
#include<irrlicht.h>
#include<exception>
#include"InputReceiver.h"
#include"ResourceManager.h"
#include<irrKlang.h>
#include"Settings.h"
#include"AudioManager.h"	

using namespace irr;
using namespace irr::core;
using namespace irr::scene;
using namespace irr::video;
using namespace irr::gui;
using namespace irr::io;
using namespace irrklang;
class Game
{
protected:
	ISoundEngine* soundEngine;
	IrrlichtDevice* device;
	ISceneManager* scene;
	IVideoDriver* driver;
	IGUIEnvironment* gui;
	ResourceManager* rcmgr;
	Settings* settings;
private:
	static Game* _instance;
public:

	Game(void);
	void Init(int width, int height);
	void Run();
	virtual void Load(){}
	virtual void Update(f32 dt){}
	virtual void Unload(){}
	~Game(void);

	static IrrlichtDevice* getDevice();
	static ISceneManager* getScene();
	static IVideoDriver* getDriver();
	static IGUIEnvironment* getGUI();
	static ResourceManager* getResMgr();
	static ISoundEngine* getSoundEngine();
	static Settings* getSettings();

};

