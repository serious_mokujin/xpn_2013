#pragma once
#include<irrlicht.h>
#include"Helper.h"
using namespace irr;
using namespace irr::scene;
using namespace irr::core;

class CameraExt
{
private:
	ICameraSceneNode* _cam;
	vector3df _nextPosition;
	vector3df _nextRotation;
	vector3df _nextTarget;
	f32 _rumbleTime;
	f32 _pushTime;
	f32 _pushInterval;
	f32 _rumbleAmount;
	f32 _rumbleStep;
	bool _rumble;

	void UpdateRumble(f32 dt);
public:
	CameraExt(ICameraSceneNode* cam);
	ICameraSceneNode* getNode();
	
	void Update(f32 dt);
	void RumblePosition(f32 seconds, f32 amount, int pushesCount);
	void RumblePosition(f32 seconds, f32 startAmount, f32 endAmount, int pushesCount);
	void StopRumble();
	void LerpPosition(vector3df next);
	vector3df getPosition();
	void setPosition(f32 x, f32 y, f32 z);
	void setPosition(vector3df pos);

	void LerpTarget(vector3df next);
	vector3df getTarget();
	void setTarget(f32 x, f32 y, f32 z);
	void setTarget(vector3df trgt);

	~CameraExt(void);
};

