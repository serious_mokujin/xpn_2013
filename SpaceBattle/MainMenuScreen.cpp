#include "MainMenuScreen.h"
#include"ScreenManager.h"

MainMenuScreen::MainMenuScreen(ScreenManager* manager):Screen(manager)
{}

void MainMenuScreen::Load()
{
	this->backTex = Game::getResMgr()->getTexture("background");
	this->_titleTex = Game::getResMgr()->getTexture("title");
	font = Game::getResMgr()->getFont("space");
	ISoundSource* s1 = Game::getResMgr()->getSound("select");
	ISoundSource* s2 = Game::getResMgr()->getSound("choose");
	_startItem = new MenuItem("START", font, MENU_ITEM_COLOR, MENU_HOVER_COLOR, MENU_SELECT_COLOR, s1, s2,vector2df(0, 300*3), SCREEN_WIDTH, 60); 
	_settingsItem = new MenuItem("SETTINGS", font, MENU_ITEM_COLOR, MENU_HOVER_COLOR, MENU_SELECT_COLOR, s1, s2,vector2df(0, 360*4), SCREEN_WIDTH, 60);
	_helpItem = new MenuItem("HELP", font, MENU_ITEM_COLOR, MENU_HOVER_COLOR, MENU_SELECT_COLOR, s1, s2,vector2df(0, 420*5), SCREEN_WIDTH, 60);
	_aboutItem = new MenuItem("ABOUT", font, MENU_ITEM_COLOR, MENU_HOVER_COLOR, MENU_SELECT_COLOR, s1, s2,vector2df(0, 480*6), SCREEN_WIDTH, 60);
	_exitItem = new MenuItem("EXIT", font, MENU_ITEM_COLOR, MENU_HOVER_COLOR ,MENU_SELECT_COLOR, s1, s2,vector2df(0, 540*7), SCREEN_WIDTH, 60);
	items->push_back(_startItem);
	items->push_back(_settingsItem);
	items->push_back(_helpItem);
	items->push_back(_aboutItem);
	items->push_back(_exitItem);
	_startItem->Link(_exitItem, _settingsItem);
	_settingsItem->Link(_startItem, _helpItem);
	_helpItem->Link(_settingsItem, _aboutItem);
	_aboutItem->Link(_helpItem, _exitItem);
	_exitItem->Link(_aboutItem, _startItem);

	_startItem->LerpTo(0, 300, 4);
	_settingsItem->LerpTo(0, 360, 4);
	_helpItem->LerpTo(0, 420, 4);
	_aboutItem->LerpTo(0, 480, 4);
	_exitItem->LerpTo(0, 540, 4);

	AudioManager::PlayMusic("menuTheme", true);
}
void MainMenuScreen::Unload(){}

void MainMenuScreen::Update(irr::f32 dt)
{
	Screen::Update(dt);
	if(_startItem->IsPressed())
	{	changeScreen(new DifficultySelectScreen(manager));}
	else if(_settingsItem->IsPressed())
	{	changeScreen(new SettingsScreen(manager));}
	else if(_helpItem->IsPressed())
	{	changeScreen(new HelpScreen(manager)); }
	else if(_aboutItem->IsPressed())
	{	changeScreen(new AboutScreen(manager)); }
	else if(_exitItem->IsPressed())
	{	changeScreen(new AproveScreen(L"Are you sure to quit?", new ExitSplashScreen(manager), new MainMenuScreen(manager),manager));}
}
void MainMenuScreen::Draw()
{
	Screen::Draw();
	auto dim = _titleTex->getSize();
	Game::getDriver()->draw2DImage(_titleTex, vector2di(40, 40), recti(0, 0, dim.Width, dim.Height), NULL, SColor(255, 255, 255, 255), true);
	Screen::DrawInterface();
}
MainMenuScreen::~MainMenuScreen(void) { }
