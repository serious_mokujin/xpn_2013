#pragma once
#include<irrlicht.h>
#include<map>
#include<string>
#include<irrKlang.h>

using namespace irr;
using namespace irr::gui;
using namespace irr::video;
using namespace irr::scene;
using namespace irrklang;
// �������� �������� � ����. ��������� ���������, ��������� � ����������� ��������.
// ������� ������� ������������� ��� ���, ��� �������� ��������� ������� �� ���� ����
class ResourceManager
{
private:
	std::map<std::string, scene::IAnimatedMesh*>* _meshes; // ��������� ��� �����
	std::map<std::string, video::ITexture*>* _textures;	// ��������� ��� �������
	std::map<std::string, gui::IGUIFont*>* _fonts;		// ������
	std::map<std::string, ISoundSource*>* _soundSources; // ����� � ������
	IrrlichtDevice* _device;
	ISoundEngine* _soundEngine;
public:
	ResourceManager(IrrlichtDevice* device, ISoundEngine* soundEngine);
	// ������ ��� ��������. ������������� ����� ������� � ������ � ��������� � ��������������� ����������.
	bool loadTexture(std::string name, irr::io::path file);
	// preload - ���������� �� ������ ����� � ������ �� ����� �������� ��� �� ����� �������� ��������������
	bool loadSound(std::string name, irr::io::path file, bool preload);
	bool loadMesh(std::string name, irr::io::path file);
	bool loadFont(std::string name, irr::io::path file);
	// ������ ��� ���������� ������ �� ������ �� �����.
	ITexture* getTexture(std::string name);
	IAnimatedMesh* getMesh(std::string name);
	IGUIFont* getFont(std::string name);
	ISoundSource* getSound(std::string name);

	// ������ ��� �������� �������� �� �����, ����� ���������� ������, ���� �� ������ �� ����� ������������ � ����
	void unloadTexture(std::string name);
	void unloadSound(std::string name);
	void unloadMesh(std::string name);
	void unloadFont(std::string name);

	~ResourceManager(void);
};

