#include "Screen.h"
#include"ScreenManager.h"

Screen::Screen(ScreenManager* manager)
{
	this->manager = manager;
	this->backTex = NULL;
	this->items = new std::list<TextItem*>();
	next = NULL;
	close = false;
}
void Screen::Draw()
{
	if(backTex)
		Game::getDriver()->draw2DImage(backTex, 
			core::rect<s32>(0,0, SCREEN_WIDTH, SCREEN_HEIGHT),
			core::rect<s32>(0,0, SCREEN_WIDTH, SCREEN_HEIGHT));
}
void Screen::DrawInterface()
{
	for (auto i = items->begin(); i != items->end(); ++i)
		(*i)->Draw();
}
void Screen::Update(f32 dt)
{
	for (auto i = items->begin(); i != items->end(); ++i)
		(*i)->Update(dt);
}
void Screen::changeScreen(Screen* scr)
{
	next = scr;
	close = true;
}
Screen::~Screen(void)
{
	for (auto i = items->begin(); i != items->end(); ++i)
		delete *i;
	delete items;
}
