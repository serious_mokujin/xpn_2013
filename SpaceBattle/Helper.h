#pragma once
#include <irrlicht.h>
#include <stdlib.h>
#include <cstdarg>
#include <vector>
#include <string>

using namespace irr::core;
using namespace irr;
class Helper
{
public:
	static void setRect(rect<s32> &a, int x, int y, int width, int height)
	{
		a.UpperLeftCorner.X = x;
		a.UpperLeftCorner.Y = y;
		a.LowerRightCorner.X = x+width;
		a.LowerRightCorner.Y = y+height;
	}
	static void setRect(rect<f32> &a, f32 x, f32 y, f32 width, f32 height)
	{
		a.UpperLeftCorner.X = x;
		a.UpperLeftCorner.Y = y;
		a.LowerRightCorner.X = x+width;
		a.LowerRightCorner.Y = y+height;
	}
	static recti ftoi(rectf &a)
	{
		return recti(a.UpperLeftCorner.X, a.UpperLeftCorner.Y, a.LowerRightCorner.X, a.LowerRightCorner.Y);
	}
	static vector2df RandomDirection2D()
	{
	  float azimuth = ((float)rand() / RAND_MAX) * 2 * PI;
	  return vector2df(cos(azimuth), sin(azimuth));
	}
	static vector3df RandomDirection3D()
	{
	  float z = (2*((float)rand() / RAND_MAX)) - 1; // z is in the range [-1,1]
	  vector2df planar = RandomDirection2D() * sqrt(1-z*z);
	  return vector3df(planar.X, planar.Y, z);
	}
	static stringw format (const wchar_t *fmt, ...)
	{
		va_list ap;
		va_start (ap, fmt);
		stringw buf = vformat (fmt, ap);
		va_end (ap);
		return buf;
	}

	static stringw vformat (const wchar_t *fmt, va_list ap)
	{
		// Allocate a buffer on the stack that's big enough for us almost
		// all the time.  Be prepared to allocate dynamically if it doesn't fit.
		size_t size = 1024;
		wchar_t stackbuf[1024];
		std::vector<wchar_t> dynamicbuf;
		wchar_t *buf = &stackbuf[0];

		while (1) {
			// Try to vsnprintf into our buffer.
			int needed = vswprintf (buf, size, fmt, ap);
			// NB. C99 (which modern Linux and OS X follow) says vsnprintf
			// failure returns the length it would have needed.  But older
			// glibc and current Windows return -1 for failure, i.e., not
			// telling us how much was needed.

			if (needed <= (int)size && needed >= 0) {
				// It fit fine so we're done.
				return stringw (buf, (size_t) needed);
			}

			// vsnprintf reported that it wanted to write more characters
			// than we allotted.  So try again using a dynamic buffer.  This
			// doesn't happen very often if we chose our initial size well.
			size = (needed > 0) ? (needed+1) : (size*2);
			dynamicbuf.resize (size);
			buf = &dynamicbuf[0];
		}
}
};

