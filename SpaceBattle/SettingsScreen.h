#pragma once
#include "screen.h"
#include "Helper.h"
#include<irrlicht.h>
#include"GlobalConst.h"
#include "MainMenuScreen.h"
using namespace irr::video;
using namespace irr;
using namespace irr::core;


class SettingsScreen :
	public Screen
{
private:
	virtual void Draw();
	IGUIFont* font;
	IGUIFont* fontBig;
	TextItem* _titleItem;
	MenuItem* _fpsItem;
	MenuItem* _soundItem;
	MenuItem* _musicItem;
	MenuItem* _soundVolumeItem;
	MenuItem* _musicVolumeItem;
	MenuItem* _backItem;
public:
	SettingsScreen(ScreenManager* manager);
	virtual void Load();
	virtual void Unload();
	virtual void Update(irr::f32 dt);
	virtual ~SettingsScreen(void);
};

