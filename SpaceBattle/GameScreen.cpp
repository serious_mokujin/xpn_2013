#include "GameScreen.h"
#include"ScreenManager.h"

GameScreen::GameScreen(ScreenManager* manager):Screen(manager)
{
	_menuShown = false;
	_pause = false;
	srand(time(0));
	InputReceiver::Subscribe(this);
}

void GameScreen::Load()
{
	Screen::Load();
	
	
	this->backTex = Game::getResMgr()->getTexture("background");
	this->_font = Game::getResMgr()->getFont("space");
	this->_fontBig = Game::getResMgr()->getFont("spaceBig");
	
	_gameStatusItem = new TextItem("", _fontBig, SColor(255, 0,  255, 128),vector2df(-SCREEN_WIDTH, 250), SCREEN_WIDTH,  64);
	_totalScoreItem = new TextItem("", _font, SColor(255, 255,  255, 255), vector2df(SCREEN_WIDTH, 300), SCREEN_WIDTH,  64);
	_scoreItem = new TextItem("score: 0", _font, SColor(255, 255,  255, 255), vector2df(10, SCREEN_HEIGHT-40), 350, 40);

	_menuItem = new MenuItem("<<  menu",_font, MENU_ITEM_COLOR, MENU_HOVER_COLOR, MENU_SELECT_COLOR, 
											Game::getResMgr()->getSound("select"),
											Game::getResMgr()->getSound("choose"),
											vector2df(-SCREEN_WIDTH*2, 550), 340, 40);
	_playItem = new MenuItem("play again", _font, MENU_ITEM_COLOR,MENU_HOVER_COLOR, MENU_SELECT_COLOR,
											Game::getResMgr()->getSound("select"),
											Game::getResMgr()->getSound("choose"),
											vector2df(SCREEN_WIDTH*3, 550), 340, 40);

	items->push_back(_gameStatusItem);
	items->push_back(_totalScoreItem);
	items->push_back(_scoreItem);
	items->push_back(_menuItem);
	items->push_back(_playItem);
	_menuItem->Link(_playItem, _playItem);
	_playItem->Link(_menuItem, _menuItem);
	_menuItem->BlockInputRecursive();

	_cam = new CameraExt(Game::getScene()->addCameraSceneNode(0, core::vector3df(0, 0, CAMERA_DISTANCE), core::vector3df(0, 0, 0)));
	_cam->getNode()->setFOV(atan(tan(SCREEN_HEIGHT/2.0f)*((float)SCREEN_HEIGHT/(float)SCREEN_WIDTH)));
	_cam->setTarget(vector3df(0, -300, 0));
	_cam->LerpTarget(vector3df(0, 0, 0));
	//const char* shader = "uniform sampler2D tex; void main(){gl_FragColor = texture2D(tex, gl_TexCoord[0].xy);}";
	//video::IGPUProgrammingServices* gpu = Game::getDriver()->getGPUProgrammingServices();
	
	level = new Level();
	ship = new Ship();
	scene::ISceneNode* light =
        Game::getScene()->addLightSceneNode(0, core::vector3df(0, 150,-50),
        video::SColorf(1.0f, 1.0f, 1.0f, 1.0f), 800000000000.0f);
	Game::getScene()->setAmbientLight(SColorf(1.0f, 1.0f, 1.0f, 1.0f));
	level->setShip(ship);
	AudioManager::PlayMusic("gameTheme", true);
}

void GameScreen::Unload()
{
	Screen::Unload();
	// �������� ���� �������� ����, ��� ��� ��� ��� ������ ����� �� �����, ���� ����� �� ������ ������ ������
	Game::getResMgr()->unloadTexture("enemy1");
	Game::getResMgr()->unloadTexture("enemy2");
	Game::getResMgr()->unloadTexture("enemy3");
	Game::getResMgr()->unloadTexture("enemy4");
	Game::getResMgr()->unloadTexture("ship");
	Game::getResMgr()->unloadTexture("rocket");
	Game::getResMgr()->unloadTexture("asteroid");
	Game::getResMgr()->unloadSound("fire");
	Game::getResMgr()->unloadSound("explosion1");
	Game::getResMgr()->unloadSound("explosion2");
	Game::getResMgr()->unloadSound("win");
	Game::getResMgr()->unloadSound("lose");

	delete ship;
	delete level;
	delete _cam;
	Rocket::ClearPool();

	AudioManager::StopMusic();
}
void GameScreen::changeVolume(f32 dt, bool mute)
{
	f32 v = AudioManager::GetMusicVolume();
	if(v > 0)
	{
		if(mute)
		{
			if(v > 0.2) v -= dt;
			AudioManager::SetMusicVolume(v);
		}
		else
		{
			if(v < 1)
				v += dt;
			AudioManager::SetMusicVolume(min(v, 1.0f));
		}
	}
}
void GameScreen::Update(f32 dt)
{
	if(rand()%50 == 0) // ������� ��������� �� ������ ����� � ��������� ����
		Asteroid::LaunchAsteroid();
	Asteroid::UpdatePool(dt);
	Screen::Update(dt);
	
	_cam->Update(dt);
	
	bool over_old = level->IsOver();		// ����������� ��������� ���� � ����� ����� ����������� ������
	int score_old = level->getScore();	// ����� ����� ������ ��� ���������� ���� ����� ������ ������ ��� ��������� �� ������ ������� �� ���� �����
	if(_menuShown) // ���� ������ � ������� ����
	{
		changeVolume(dt, true);
		if(_menuItem->IsPressed() || _playItem->IsPressed())
		{
			returnToMenu = _menuItem->IsPressed();
			hideInGameMenu(!returnToMenu);
		}
	} // ���� ��� �� � ����, �� ��� ��������, � ������� ��� �� �����
	else if(_timeout > 0) 
	{
		changeVolume(dt, false);
		_timeout -= dt; // ���������� ��������
		if(_timeout <= 0)
		{
			_pause = false; 
			if(returnToMenu)
				changeScreen(new MainMenuScreen(manager));
			else
			{
				_cam->LerpTarget(vector3df(0, 0, 0));
				if(level->IsOver())
				{
					level->Rebuild();
					delete ship;
					ship = new Ship();
					level->setShip(ship);
				}
			}
		}
		
	}
	if(!_pause)
	{
		level->Update(dt);
		if(level->IsAction())
			_cam->RumblePosition(1, 5, 0, 50);
		if(score_old != level->getScore())
		{
			stringw s = "score: "; s += level->getScore();
			_scoreItem->setText(s);
		}
	}
	if(level->IsOver())
	{
		if(!over_old)
		{
			showInGameMenu();
			_cam->setTarget(vector3df(0, 0, 0));
			_cam->StopRumble();
			_cam->LerpTarget(vector3df(0, -300, 0));
		}	
	}
		
	Rocket::UpdatePool(dt);
}
void GameScreen::OnPress(EKEY_CODE e)
{
	if(!level->IsOver() && e == KEY_ESCAPE)
	{
		if(_menuShown) 
		{
			returnToMenu = false;
			hideInGameMenu(true); 
		}
		else 
		{
			showInGameMenu();
			_playItem->setText("continue");
			_pause = true;
		}
		
	}
}
void GameScreen::showInGameMenu()
{
	_gameStatusItem->setPosition(-SCREEN_WIDTH, 250);
	_totalScoreItem->setPosition(SCREEN_WIDTH, 300);
	_scoreItem->setPosition(10, SCREEN_HEIGHT-40);
	_menuItem->setPosition(-SCREEN_WIDTH*2, 550);
	_playItem->setPosition(SCREEN_WIDTH*3, 550);
	_gameStatusItem->setText((level->IsOver() ? (level->IsWin() ? L"PLANET SAVED!" : L"PLANET INVADED!") : L"PAUSE"));
	_gameStatusItem->setColor(SColor(255,level->IsWin() ? 0 : 255, level->IsWin() ? 255 : 0, 128));
	_gameStatusItem->LerpTo(0, _gameStatusItem->getPosition().Y, 2);

	_totalScoreItem->setText(Helper::format((level->IsOver() ? L"Total score: %d" : L"Score: %d"), level->getScore()));
	_totalScoreItem->LerpTo(0, _totalScoreItem->getPosition().Y,2);
	_scoreItem->setText(Helper::format(L"score: %d", level->getScore()));
	_scoreItem->LerpTo(_scoreItem->getPosition().X, SCREEN_HEIGHT, 2);

	_playItem->LerpTo(SCREEN_WIDTH-380, _playItem->getPosition().Y, 2);
	_playItem->setText("play again");
	_menuItem->LerpTo(40, _menuItem->getPosition().Y, 2);
	_menuItem->UnblockInputRecursive();
	_menuShown = true;
}
void GameScreen::hideInGameMenu(bool showScore)
{
	_gameStatusItem->LerpTo(SCREEN_WIDTH, _gameStatusItem->getPosition().Y, 2);
	_totalScoreItem->LerpTo(-SCREEN_WIDTH, _totalScoreItem->getPosition().Y, 2);
	_playItem->LerpTo(SCREEN_WIDTH*3, _playItem->getPosition().Y, 2);
	_menuItem->LerpTo(-SCREEN_WIDTH*2, _menuItem->getPosition().Y, 2);
	if(showScore)
	{
		_scoreItem->LerpTo(_scoreItem->getPosition().X, SCREEN_HEIGHT-40);
		_scoreItem->setText("score: 0");
	}
	_menuItem->BlockInputRecursive();
	_menuShown = false;
	_timeout = 1;
}


void GameScreen::Draw()
{
	Screen::Draw();
	Game::getScene()->drawAll();
	Screen::DrawInterface();
}
GameScreen::~GameScreen(void)
{
	Asteroid::ClearPool();
	InputReceiver::Unscribe(this);
}
