#pragma once
#include <irrlicht.h>
#include<stdlib.h>
#include<list>
#include "IKeyListener.h"
#include"IMouseListener.h"

using namespace irr;

class InputReceiver :
	public IEventReceiver
{
private:
	bool _keyArray[KEY_KEY_CODES_COUNT];
	irr::core::vector2di _mprevpos;
	irr::core::vector2di _mpos;
	bool _lbtn;
	bool _mbtn;
	bool _rbtn;
	std::list<IKeyListener*> keyListeners;
	std::list<IMouseListener*> mouseListeners;

	static InputReceiver* instance;

	InputReceiver(void)
	{
	}

public:
	
	static void CreateInstance(IrrlichtDevice* device)
	{
		instance = new InputReceiver();
		device->setEventReceiver(instance);
		memset(instance->_keyArray, 0, KEY_KEY_CODES_COUNT);
		instance->_lbtn = false;
		instance->_rbtn = false;
		instance->_mbtn = false;
	}
	static void DestroyInstance()
	{
		delete instance;
	}
	static bool IsKeyDown(EKEY_CODE keyCode)
    {
        return instance->_keyArray[keyCode];
    }
	static core::vector2di getMousePosition()
	{
		return instance->_mpos;
	}
	static core::vector2di getMousePreviousPosition()
	{
		return instance->_mprevpos;
	}
	static bool IsMouseLeftDown(){return instance->_lbtn;}
	static bool IsMouseMiddleDown(){return instance->_mbtn;}
	static bool IsMouseRightDown(){return instance->_rbtn;}

	virtual bool OnEvent(const SEvent& event)
    {
        // Remember whether each key is down or up
        if (event.EventType == irr::EET_KEY_INPUT_EVENT)
		{
			_keyArray[event.KeyInput.Key] = event.KeyInput.PressedDown;
			for (std::list<IKeyListener*>::iterator i = keyListeners.begin(); i != keyListeners.end(); i++)
			{
				if(event.KeyInput.PressedDown)
					(*i)->OnPress(event.KeyInput.Key);
				else
					(*i)->OnRelease(event.KeyInput.Key);
			}
		}
		if (event.EventType == irr::EET_MOUSE_INPUT_EVENT)
        {
            switch(event.MouseInput.Event)
            {
            case EMIE_LMOUSE_PRESSED_DOWN:
				_lbtn = true;
                break;
            case EMIE_LMOUSE_LEFT_UP:
                _lbtn = false;
                break;
			case EMIE_RMOUSE_PRESSED_DOWN:
				_rbtn = true;
				break;
			case EMIE_RMOUSE_LEFT_UP:
				_rbtn = false;
				break;
			case EMIE_MMOUSE_PRESSED_DOWN:
				_mbtn = true;
				break;
			case EMIE_MMOUSE_LEFT_UP:
				_mbtn = false;
				break;
            case EMIE_MOUSE_MOVED:
				_mprevpos = _mpos;
                _mpos.X = event.MouseInput.X;
                _mpos.Y = event.MouseInput.Y;
                break;

            default:
                // We won't use the wheel
                break;
            }
			for (std::list<IMouseListener*>::iterator i = mouseListeners.begin(); i != mouseListeners.end(); i++)
			{
				(*i)->OnEvent(event.MouseInput.Event);
			}
        }
        return false;
    }

	static void Subscribe(IKeyListener* keyListener)
	{
		instance->keyListeners.push_back(keyListener);
	}
	static void Subscribe(IMouseListener* mouseListener)
	{
		instance->mouseListeners.push_back(mouseListener);
	}
	static void Unscribe(IKeyListener* keyListener)
	{
		instance->keyListeners.remove(keyListener);
	}
	static void Unscribe(IMouseListener* mouseListener)
	{
		instance->mouseListeners.remove(mouseListener);
	}


	~InputReceiver(void)
	{}
};


