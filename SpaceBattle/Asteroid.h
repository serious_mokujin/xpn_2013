#pragma once
#include "gameobject.h"
#include<irrlicht.h>
#include "GlobalConst.h"
using namespace irr;
using namespace irr::core;

class Asteroid :
	public GameObject
{
private:
	bool _active;
	vector3df _speed;
	static std::list<Asteroid*> pool;
	void Launch();
public:
	Asteroid(void);
	virtual void Update(f32 dt);

	static void LaunchAsteroid();
	static void UpdatePool(f32 dt);
	static void ClearPool();

	virtual ~Asteroid(void);
};

