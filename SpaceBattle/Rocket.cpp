#include "Rocket.h"
std::list<Rocket*> Rocket::pool;

Rocket::Rocket():GameObject("rocket")
{
	_active = false;
	_fireSound = Game::getResMgr()->getSound("fire");
}
void Rocket::Update(f32 dt)
{
	if(_active)
	{
		_speed += dt*_accel;
		vector3df p = _node->getPosition();
		p.Y += _speed*dt;
		_node->setPosition(p);
		if(p.Y < -300 || p.Y > 300)
		{
			Frezee();
		}
	}
}
void Rocket::Frezee()
{
	_node->setPosition(vector3df(-500, -500, -500));
	
	_active = false;
	_node->setVisible(false);
}
void Rocket::UpdatePool(f32 dt)
{
	for(std::list<Rocket*>::iterator i = pool.begin(); i != pool.end(); ++i)
		(*i)->Update(dt);
}
void Rocket::ClearPool()
{
	for(std::list<Rocket*>::iterator i = pool.begin(); i != pool.end(); ++i)
		delete (*i);
	pool.clear();
}
void Rocket::LaunchRocket(vector3df pos, bool directUp)
{
	for(std::list<Rocket*>::iterator i = pool.begin(); i!= pool.end(); ++i)
		if(!(*i)->_active)
		{
			(*i)->Launch(pos, directUp);

			return;
		}
	Rocket* r = new Rocket();
	pool.push_back(r);
	r->Launch(pos, directUp);
}

void Rocket::Launch(vector3df pos, bool directUp)
{
	_node->setPosition(pos);
	if(directUp)
		_node->setRotation(vector3df(0, 0, 0));
	else
		_node->setRotation(vector3df(0, 0, 180));
	_speed = 40;
	_accel = directUp ? 200.0f : -200.0f;
	_active = true;
	_node->setVisible(true);
	_node->updateAbsolutePosition();
	addFirePS(vector3df(0, -10, 0));
	AudioManager::PlaySound(_fireSound);
}

void Rocket::addFirePS(vector3df pos)
{
	ISceneManager* scene = Game::getScene();
    IParticleSystem* system = createParticleSystem(_node, scene, 6);
    system->setPosition(pos);
    system->setScale(vector3df(1.0f));
    system->updateAbsolutePosition();

    particle::IParticleDrawer* drawer = system->addParticleDrawer();
	
	for (int j = 0; j < 2; j++)
	{	
		for (int i = 0; i < 8; i++)
		{
			drawer->addUVCoords(particle::SParticleUV(vector2df( i * 0.125f, j * 0.125f), 
											vector2df((i*0.125f)+0.125f, j*0.125f), 
											vector2df(i*0.125f,(j*0.125f) + 0.125f),
											vector2df((i*0.125f)+0.125f,(j*0.125f) + 0.125f)));
		}
	}
    particle::IParticleEmitter* _emitter = drawer->addStandardEmitter(vector3df(0,0,0), 
																		vector3df(0.1,0,0), 
																		vector3df(0,1,0),
																		vector3di(45, 0, 45), 50, 80, 200, 250, 400,
																		vector2df(6), 
																		vector2df(10), 
																		vector2df(2), SColor(255,220,220,0), SColor(255,255,50,0));
	
    particle::IParticleAffector* affector = new ColorAffectorQ(irr::video::SColor(255, 100, 0, 0), irr::video::SColor(0,0,0,0));
    drawer->addAffector(affector);
    affector->drop();
    affector = new GravityAffector(irr::core::vector3df(0.f,-100.0f,0.f));
    drawer->addAffector(affector);
    affector->drop();

	///Smoke Trails

    //emitter = drawer->addStandardEmitter(vector3df(0,0,0), vector3df(0,0,0), vector3df(0,10,0), vector3di(180, 0, 180), 0, 500, 10, 400, 600, vector2df(0.25,1.0), vector2df(0.25,1.0), vector2df(2.f), SColor(255,255,128,50), SColor(255,255,128,50));

    SMaterial overrideMaterial;
    overrideMaterial.MaterialType = EMT_TRANSPARENT_ADD_COLOR;
    overrideMaterial.setTexture(0, Game::getResMgr()->getTexture("particles"));
    overrideMaterial.setFlag(EMF_LIGHTING, false);
    overrideMaterial.setFlag(EMF_ZWRITE_ENABLE, true);
    overrideMaterial.setFlag(EMF_BACK_FACE_CULLING, false);
    overrideMaterial.MaterialTypeParam = pack_textureBlendFunc (EBF_SRC_ALPHA, EBF_ONE_MINUS_SRC_ALPHA, EMFN_MODULATE_1X, EAS_VERTEX_COLOR | EAS_TEXTURE );
    system->setOverrideMaterial(overrideMaterial);

	irr::scene::ISceneNodeAnimator* anim = scene->createDeleteAnimator(3000);
    system->addAnimator(anim);
    anim->drop();
	system->drop();
}

Rocket::~Rocket(void)
{
	
}
