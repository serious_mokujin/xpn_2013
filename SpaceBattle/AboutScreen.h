#pragma once
#include "screen.h"
#include "Helper.h"
using namespace irr;
using namespace irr::core;

class AboutScreen :
	public Screen
{
private:
	ITexture* _texture;
	virtual void Draw();
	MenuItem* _backItem;
public:
	AboutScreen(ScreenManager* manager);
	~AboutScreen(void);
	virtual void Load();
	virtual void Update(f32 dt);
};

