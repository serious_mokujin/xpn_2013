#include "MenuItem.h"


MenuItem::MenuItem(stringw text, 
		IGUIFont* font, 
		SColor color,
		SColor selectColor, 
		SColor chooseColor, 
		ISoundSource* selectSound, 
		ISoundSource* chooseSound,
		vector2df pos, int width, int height):TextItem(text, font, color, pos, width, height)
{
	this->_chooseColor = chooseColor;
	this->_selectedColor = selectColor;
	this->_selectSound = selectSound;
	this->_chooseSound = chooseSound;
	this->_next = NULL;
	this->_prev = NULL;
	this->_selected = false;
	this->_pressed = false;
	this->_blockInput = false;
	InputReceiver::Subscribe((IMouseListener*)this);
	InputReceiver::Subscribe((IKeyListener*)this);
}
void MenuItem::Link(MenuItem* prev, MenuItem* next) { _prev = prev; _next = next; }
MenuItem* MenuItem::getNext() { return _next; }
MenuItem* MenuItem::getPrev() { return _prev; }
void MenuItem::Select()
{
	MenuItem* i = _next;
	while(i != this && i != NULL)
	{
		i->Unselect();
		i->Release();
		i = i->_next;
	}
	if(!this->_selected)
		AudioManager::PlaySound(_selectSound);
	this->_selectLock = true; // ��������� ���������� ������.
	this->_selected = true;
}
void MenuItem::Unselect() { this->_selected = false; }
void MenuItem::Press() 
{
	MenuItem* i = _next;
	while(i != this && i != NULL)
	{
		i->Release();
		i = i->_next;
	}
	if(!this->_pressed)
		AudioManager::PlaySound(_chooseSound);
	this->_pressed = true; 
}
void MenuItem::Release() { this->_pressed = false; }

bool MenuItem::IsSelected() { return _selected; }
bool MenuItem::IsPressed() { return _pressed; }

void MenuItem::setSelectColor(SColor color) { this->_selectedColor = color; }
SColor MenuItem::getSelectColor() { return this->_selectedColor; }
SColor MenuItem::getChooseColor() {	return _chooseColor; }
void MenuItem::setChooseColor(SColor color) { this->_chooseColor = color; }
void MenuItem::setChooseSound(ISoundSource* sound) { this->_chooseSound = sound; }
void MenuItem::setSelectSound(ISoundSource* sound) { this->_selectSound = sound; }

void MenuItem::Draw()
{
	font->draw(text, getRect(),(_selected?(_pressed ? _chooseColor : _selectedColor) : color), true, true);
}
void MenuItem::Update(f32 dt)
{
	TextItem::Update(dt);
	_selectLock = false; // ������ ���������� ������.
}
void MenuItem::UpdateRecursive(f32 dt)
{
	MenuItem* i = _next;
	while(i != this && i != NULL)
	{
		i->Update(dt);
		i = i->_next;
	}
	this->Update(dt);
}
void MenuItem::DrawRecursive()
{
	MenuItem* i = _next;
	while(i != this && i != NULL)
	{
		i->Draw();
		i = i->_next;
	}
	this->Draw();
}
void MenuItem::DeleteRecursive()
{
	MenuItem* i = _next;
	while(i != this && i != NULL)
	{
		MenuItem* temp = i->_next;
		delete i;
		i = temp;
	}
}

void MenuItem::OnPress(EKEY_CODE e)
{
	if(_blockInput) return;
	if(_selected)
	{
		if(e == KEY_RETURN)
			Press();
		if(e == KEY_DOWN || e == KEY_RIGHT)
			if(!_selectLock)
				_next->Select();
		if(e == KEY_UP || e == KEY_LEFT)
			if(!_selectLock)
				_prev->Select();
	}
	else
	{
		MenuItem* i = this->_next; 
		while(i != this && i != NULL)
		{
			if(i->_selected) return;
			i = i->_next;
		}
		Select();
	}
}
void MenuItem::OnRelease(EKEY_CODE e){}
void MenuItem::OnEvent(EMOUSE_INPUT_EVENT e)
{
	if(_blockInput) return;
	if(e == EMOUSE_INPUT_EVENT::EMIE_MOUSE_MOVED)
	{
		if(getRect().isPointInside(InputReceiver::getMousePosition()))
			Select();
		else 
			_selected = false;
	}
	if(e == EMOUSE_INPUT_EVENT::EMIE_LMOUSE_LEFT_UP)
	{
		Release();
	}
	if(_selected && e == EMOUSE_INPUT_EVENT::EMIE_LMOUSE_PRESSED_DOWN)
		Press();
}
void MenuItem::BlockInputRecursive() 
{ 
	MenuItem* i = this->_next; 
	while(i != this && i != NULL)
	{
		i->Release();
		i->Unselect();
		i->_blockInput = true;
		i = i->_next;
	}
	_blockInput = true; 
}
void MenuItem::UnblockInputRecursive() 
{ 
	MenuItem* i = this->_next; 
	while(i != this && i != NULL)
	{
		i->_blockInput = false;
		i = i->_next;
	}
	_blockInput = false;  
}
MenuItem::~MenuItem(void)
{
	InputReceiver::Unscribe((IMouseListener*)this);
	InputReceiver::Unscribe((IKeyListener*)this);
}
