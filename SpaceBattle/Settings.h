#pragma once
#include <irrlicht.h>
#include "AudioManager.h"
using namespace irr;
using namespace irr::core;
enum DIFFICULTY_LEVEL { EASY = 2, MEDIUM = 4, HARD = 5, CRAZY = 8 };

// �����, �������������� ���������, ������� ����� ����� �������� � ����
class Settings
{
public:
	bool fpsVisible; // ���\���� ����������� �������� ������ �� ������
	DIFFICULTY_LEVEL difficulty; // ��������� ����
	Settings(bool showFPS = false, bool sounds = true, bool music = true, f32 soundVol = 1.0f, f32 musicVol = 1.0f):
		fpsVisible(showFPS) 
	{
		difficulty = DIFFICULTY_LEVEL::EASY;
	}
	// ��������, �������� �� �����
	bool getSoundsState(){return AudioManager::isPlaySounds();}
	// ��������/��������� �����
	void setSoundsState(bool playSounds){ AudioManager::setPlaySounds(playSounds); }
	// ��������, �������� �� ������
	bool getMusicState(){return AudioManager::isPlayMusic();}
	// ��������/��������� ������
	void setMusicState(bool playMusic){ AudioManager::setPlayMusic(playMusic); }
	// �������� ��������� ������
	f32 getSoundVolume(){return AudioManager::GetSoundVolume();}
	// ������������� ��������� ������
	void setSoundVolume(f32 volume)
	{
		f32 _soundVolume = 0.0f;
		if(volume > 1.0f && volume < 1.1f) _soundVolume = 1.0f;
		else if(volume >= 1.1f) _soundVolume = 0.1f;
		else _soundVolume = volume;
		AudioManager::SetSoundVolume(_soundVolume);
	}
	// �������� ��������� ������
	f32 getMusicVolume(){return AudioManager::GetMusicVolume();}
	// ������������� ��������� ������
	void setMusicVolume(f32 volume)
	{
		f32 _musicVolume = 0.0f;
		if(volume > 1.0f && volume < 1.1f) _musicVolume = 1.0f;
		else if(volume >= 1.1f) _musicVolume = 0.1f;
		else _musicVolume = volume;
		AudioManager::SetMusicVolume(_musicVolume);
	}
	~Settings(void){}
};

