#pragma once
#include "screen.h"
#include<irrlicht.h>
#include"Level.h"
#include"GlobalConst.h"
#include"MainMenuScreen.h"
#include"CameraExt.h"
#include"Asteroid.h"
#include<stdlib.h>
#include<time.h>
#include"IKeyListener.h"

using namespace irr;
using namespace irr::video;
using namespace irr::core;

class GameScreen :
	public Screen, IKeyListener
{
private:
	f32 _timeout; // ������� ��� ����, ����� �������� ����
	bool returnToMenu;
	bool _pause;
	Level* level;
	Ship* ship;
	IParticleSystemSceneNode* particleSystem;
	IGUIFont* _font;
	IGUIFont* _fontBig;
	TextItem* _gameStatusItem;
	TextItem* _totalScoreItem;
	TextItem* _scoreItem;
	MenuItem* _menuItem;
	MenuItem* _playItem;

	CameraExt* _cam;
	bool _menuShown;
	void showInGameMenu();
	void hideInGameMenu(bool showScore);
	virtual void Draw();
	void changeVolume(f32 dt, bool mute);
	
public:
	GameScreen(ScreenManager* manager);
	virtual void Load();
	virtual void Unload();
	virtual void Update(f32 dt);
	virtual void OnPress(EKEY_CODE e);
	
	virtual ~GameScreen(void);
};

