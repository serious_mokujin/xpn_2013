#pragma once
#include<irrlicht.h>
#include"Enemy.h"
#include"Ship.h"
#include<vector>
#include"ResourceManager.h"
#include"ScoreContainer.h"

using namespace irr;

// �������� �����, ���������� ���������� �� ���� � ������������� �������� ������ � �����
class Level
{
private:
	vector<Enemy*>* _enemies;
	int _width, _height;
	f32 _enemiesSpeed;
	Ship* _ship;
	bool _action; // ������ ������ :) ���������, �� ��, ��� ����� ������ ������
	bool _moveDirection; // ����������� �������� ������. true - ������
	bool _newDirection; // ��������������� ���������� ��� ��������� ����������� �������� ������
	void UpdateEnemies(f32 dt); // ���������� ��������� ������
	void MoveEnemies(f32 dt); // ��������� �������� �����
	bool _win;
	bool _game;
	void gameOver(bool win);
	ScoreContainer score;
	f32 _pushDown;		// ������� ����, �� ������� �������� ���� ������
public:
	Level(); 
	void Rebuild(); // ���������� ������. ����������� ������ � ����� �����.
	void Update(f32 dt); // ���������� ��������� ������. dt - �������� ������� ����� ������
	void setShip(Ship* ship); // ��������� ������� ������ �� �������
	bool IsOver() const; // ����������, ��������� �� ����. true - ���� ���������
	bool IsWin() const; // ����������, ������ ��. true - ���� ��������
	bool IsAction() const; // ���������, ��� � ������ ������ ����� ������ ������, ��� ������ ����������
	int getScore();	// ��������� ������� �����
	Ship* getShip() const; // ��������� ������������� ������� �� �������

	virtual ~Level(void);
};

