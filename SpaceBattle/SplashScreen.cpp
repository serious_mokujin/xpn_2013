#include "SplashScreen.h"
#include"MainMenuScreen.h"
#include"ScreenManager.h"

SplashScreen::SplashScreen(ScreenManager* manager):Screen(manager)
{
	_timeLeft = 3;
}

void SplashScreen::Load()
{
	Game::getResMgr()->loadTexture("splash","content/textures/splash.png");
	this->backTex = Game::getResMgr()->getTexture("splash");
}
void SplashScreen::Update(irr::f32 dt)
{
	if(_timeLeft > 0)
		_timeLeft -= dt;
	else 
	{
		changeScreen(new MainMenuScreen(manager));
	}
}
void SplashScreen::Unload()
{
	Game::getResMgr()->unloadTexture("splash");
}
SplashScreen::~SplashScreen(void)
{
}
