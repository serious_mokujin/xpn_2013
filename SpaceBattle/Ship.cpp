#include "Ship.h"


Ship::Ship() : GameObject("ship")
{
	_speed = 125;
	_node->setPosition(irr::core::vector3df(0, LINE_OF_SHIP*2, -350));
	InputReceiver::Subscribe((IMouseListener*)this);
	_node->setMaterialFlag(E_MATERIAL_FLAG::EMF_LIGHTING, true);
	_rocketManic = new Rocket();
	manicAnimation = 0;
	_destroyed = false;

	_destroyAccel = vector3df(0, -50, 0);
	_destroyRotation = vector3df(((rand()%20)/10.0f),((rand()%20)/10.0f), ((rand()%20)/10.0f));
	int c = rand()%3;
	if(c == 0) _rotation.X = 0;
	else if(c == 1) _rotation.Y = 0;
	else _rotation.Z = 0;
	_win = false;

	_moveLeft = false;
	_moveRight = false;
	_mouseControll = false;
	_psysteml = createFirePS(vector3df(-5, -15, 0));
	_psystemr = createFirePS(vector3df(5, -15, 0));
}

IParticleSystem* Ship::createFirePS(vector3df pos)
{
	ISceneManager* scene = Game::getScene();
    IParticleSystem* system = createParticleSystem(_node, scene, 6);
    system->setPosition(pos);
    system->setScale(vector3df(1.0f));
    system->updateAbsolutePosition();

    particle::IParticleDrawer* drawer = system->addParticleDrawer();
	
	for (int j = 0; j < 2; j++)
	{	
		for (int i = 0; i < 8; i++)
		{
			drawer->addUVCoords(particle::SParticleUV(vector2df( i * 0.125f, j * 0.125f), 
											vector2df((i*0.125f)+0.125f, j*0.125f), 
											vector2df(i*0.125f,(j*0.125f) + 0.125f),
											vector2df((i*0.125f)+0.125f,(j*0.125f) + 0.125f)));
		}
	}
    particle::IParticleEmitter* _emitter = drawer->addStandardEmitter(vector3df(0,0,0), 
																		vector3df(0.1,0,0), 
																		vector3df(0,1,0),
																		vector3di(45, 0, 45), 10, 20, 1000, 250, 400,
																		vector2df(6), 
																		vector2df(10), 
																		vector2df(2), SColor(255,220,220,0), SColor(255,255,50,0));
	
    particle::IParticleAffector* affector = new ColorAffectorQ(irr::video::SColor(255, 100, 0, 0), irr::video::SColor(0,0,0,0));
    drawer->addAffector(affector);
    affector->drop();
    affector = new GravityAffector(irr::core::vector3df(0.f,-100.0f,0.f));
    drawer->addAffector(affector);
    affector->drop();

	///Smoke Trails

    //emitter = drawer->addStandardEmitter(vector3df(0,0,0), vector3df(0,0,0), vector3df(0,10,0), vector3di(180, 0, 180), 0, 500, 10, 400, 600, vector2df(0.25,1.0), vector2df(0.25,1.0), vector2df(2.f), SColor(255,255,128,50), SColor(255,255,128,50));

    SMaterial overrideMaterial;
    overrideMaterial.MaterialType = EMT_TRANSPARENT_ADD_COLOR;
    overrideMaterial.setTexture(0, Game::getResMgr()->getTexture("particles"));
    overrideMaterial.setFlag(EMF_LIGHTING, false);
    overrideMaterial.setFlag(EMF_ZWRITE_ENABLE, true);
    overrideMaterial.setFlag(EMF_BACK_FACE_CULLING, false);
    overrideMaterial.MaterialTypeParam = pack_textureBlendFunc (EBF_SRC_ALPHA, EBF_ONE_MINUS_SRC_ALPHA, EMFN_MODULATE_1X, EAS_VERTEX_COLOR | EAS_TEXTURE );
    system->setOverrideMaterial(overrideMaterial);
	
	return system;
}
void Ship::handleInput(f32 dt)
{
	if(_moveLeft)
	{
		_position.X -= dt*_speed;
		_position.X = max<f32>(_position.X, LEVEL_LEFT_SIDE);

		_rotation.Y = lerp<f32>(_rotation.Y, 15.0f, dt*3); 
	}
	else if(_moveRight)
	{
		_position.X += dt*_speed;
		_position.X = min<f32>(_position.X, LEVEL_RIGHT_SIDE);
		_rotation.Y = lerp<f32>(_rotation.Y, -15.0f, dt*3); 
	}
	else
	{
		_rotation.Y = lerp<f32>(_rotation.Y, 0.0f, dt*5); 
	}
	_moveLeft = false;
	_moveRight = false;

}
void Ship::OnEvent(EMOUSE_INPUT_EVENT e)
{
	if(e == EMOUSE_INPUT_EVENT::EMIE_MOUSE_MOVED)
	{
		_mouseNormalPosition = (f32)InputReceiver::getMousePosition().X / (f32)SCREEN_WIDTH;
		_mouseControll = true;
	}
}
f32 Ship::getNormalPosition()
{
	f32 x = _node->getPosition().X;
	x -= LEVEL_LEFT_SIDE;
	x /= (f32)(LEVEL_RIGHT_SIDE - LEVEL_LEFT_SIDE);
	return x;
}
void Ship::Destroy()
{
	_destroyed = true;
	_destroyDir.Y = 80;
	_destroyDir.Z = -150;
	_destroyDir.X = rand()%20 -10;	
	_destroyed = true;
	_speed = 10.0f;

	delete _rocketManic;
	_rocketManic = NULL;
}
void Ship::FlyAway()
{
	delete _rocketManic;
	_rocketManic = NULL;
	_win = true;
}
bool Ship::IsDestroyed()
{
	return _destroyed;
}
void Ship::Update(f32 dt)
{
	if(_node)
	{
		_position = _node->getPosition();
		_rotation = _node->getRotation();
		if(_destroyed)
		{
			_speed -= dt;

			_destroyDir.X += _destroyAccel.X*dt;
			_destroyDir.Y += _destroyAccel.Y*dt;
			_destroyDir.Z += _destroyAccel.Z*dt;

			_position += _destroyDir*dt;
			_rotation += _destroyRotation*_speed;
		}
		else if(_win)
		{
			_position.X = lerp<f32>(_position.X, 0, dt*4);
			_position.Z += dt*100;
			_position.Y -= dt*5;
			_rotation = lerp(_rotation, vector3df(0), dt*10);
		}
		else
		{
			if(InputReceiver::IsMouseLeftDown() || InputReceiver::IsKeyDown(EKEY_CODE::KEY_SPACE)) fire();
			if(InputReceiver::IsKeyDown(KEY_LEFT) || InputReceiver::IsKeyDown(KEY_KEY_A)) { _moveLeft = true; _mouseControll = false; }
			if(InputReceiver::IsKeyDown(KEY_RIGHT) || InputReceiver::IsKeyDown(KEY_KEY_D)) { _moveRight = true; _mouseControll = false; }
			if(_mouseControll)
			{
				if(_mouseNormalPosition < getNormalPosition())
					_moveLeft = true;
				else
					_moveRight = true;
				if(abs(_mouseNormalPosition - getNormalPosition())<0.005)
					_moveLeft = _moveRight = false;
			}
			handleInput(dt);

			_position.Y = lerp<f32>(_position.Y, LINE_OF_SHIP, dt*2);
			_position.Z = lerp<f32>(_position.Z, -10, dt*2);
			if(_recoilTime > 0)
			{
				_recoilTime-=dt;
				_position.Y = lerp<double>(_position.Y, LINE_OF_SHIP-20, dt*15);
			}
		}
		if(_rocketManic)
			updateManic(dt);
		_node->setPosition(_position);
		_node->setRotation(_rotation); 

		if(_position.Y < BOTTOM_DEATH_LINE)
		{
			_node->remove();
			_node = NULL;
		}
	}
	
}
void Ship::updateManic(f32 dt)
{
	vector3df rocketPos = _node->getPosition();
	if(manicAnimation < 1)
		manicAnimation += dt;
	_rocketManic->setPosition(rocketPos.X + lerp<f32>(0, (_rocketSide ? 15.0f: -15.0f), manicAnimation), rocketPos.Y - 5, rocketPos.Z+4); 
}
void Ship::fire()
{
	if(!_node) return;
	if(manicAnimation >= 1)
	{
		vector3df p = _node->getPosition();
		p.X += _rocketSide ? 15 : -15;
		p.Y -= 5;
		p.Z = 0;
		_rocketSide = !_rocketSide;
		manicAnimation = 0;
		_recoilTime = 0.2f;
		Rocket::LaunchRocket(p, true);
	}
}
Ship::~Ship(void)
{
	_psysteml->remove();
	_psystemr->remove();
	_psysteml->drop();
	_psystemr->drop();
	if(_rocketManic)
		delete _rocketManic;
	InputReceiver::Unscribe((IMouseListener*)this);
}
