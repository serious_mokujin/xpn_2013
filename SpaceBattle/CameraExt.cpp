#include "CameraExt.h"


CameraExt::CameraExt(ICameraSceneNode* cam)
{
	this->_cam = cam;
	_nextPosition = _cam->getPosition();
}
void CameraExt::Update(f32 dt)
{
	if(_rumble)
		UpdateRumble(dt);
	vector3df p = _cam->getPosition();
	p = lerp<vector3df>(p, _nextPosition, dt*20);
	_cam->setPosition(p);

	vector3df t = _cam->getTarget();
	t = lerp<vector3df>(t, _nextTarget, dt);
	_cam->setTarget(t);
	if(_rumble)
		_cam->setTarget(vector3df(p.X, p.Y, _cam->getTarget().Z));
}
void CameraExt::StopRumble() { _rumble = false; }
ICameraSceneNode* CameraExt::getNode() { return _cam; }
void CameraExt::UpdateRumble(f32 dt)
{
    if (_rumbleTime >= 0)
    {
        _rumbleTime -= dt;
        _pushTime += dt;
        if (_pushTime >= _pushInterval)
        {
            vector3df direction = Helper::RandomDirection3D();
                        
            _rumbleAmount += _rumbleStep;
            direction *= _rumbleAmount;
                        
            _cam->setPosition(_cam->getPosition() + direction);
            _cam->updateAbsolutePosition();
            _pushTime -= _pushInterval;
        }
    }
    else
        _rumble = false;
}

void CameraExt::RumblePosition(f32 seconds, f32 amount, int pushesCount) { RumblePosition(seconds, amount, amount, pushesCount); }

void CameraExt::RumblePosition(f32 seconds, f32 startAmount, f32 endAmount, int pushesCount)
{
    _rumble = true;
    _rumbleTime = seconds;
    _rumbleAmount = startAmount;
    _pushInterval = seconds / pushesCount;
    _rumbleStep = (endAmount - startAmount) / pushesCount;
	_pushTime = 0;
}

vector3df CameraExt::getTarget() { return _cam->getTarget(); }
void CameraExt::setTarget(f32 x, f32 y, f32 z) { _cam->setTarget(vector3df(x,y,z)); }
void CameraExt::setTarget(vector3df trgt) { _cam->setTarget(trgt); }

void CameraExt::LerpTarget(vector3df next) { _nextTarget = next; }


CameraExt::~CameraExt(void)
{
}
