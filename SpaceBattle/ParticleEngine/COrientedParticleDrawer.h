#ifndef CORIENTEDPARTICLEDRAWER_H
#define CORIENTEDPARTICLEDRAWER_H

#include "CParticleDrawer.h"


class COrientedParticleDrawer : public CParticleDrawer
{
    public:
        COrientedParticleDrawer();
        virtual ~COrientedParticleDrawer();

        irr::scene::particle::E_PARTICLE_DRAWER_TYPE getType() const
        {
            return irr::scene::particle::EPDT_ORIENTED;
        }

        void createParticle(const irr::u32& id, const irr::scene::particle::Particle* particle, const irr::core::vector3df& view, const irr::core::matrix4& transform);
    protected:
    private:
};

#endif // CORIENTEDPARTICLEDRAWER_H
