#include "HelpScreen.h"
#include"ScreenManager.h"

HelpScreen::HelpScreen(ScreenManager* manager):Screen(manager)
{
}
void HelpScreen::Load()
{
	this->backTex = Game::getResMgr()->getTexture("background");
	this->_texture = Game::getResMgr()->getTexture("help");
	_backItem = new MenuItem("back", Game::getResMgr()->getFont("space"), 
							MENU_ITEM_COLOR, MENU_HOVER_COLOR, MENU_SELECT_COLOR, 
							Game::getResMgr()->getSound("select"),
							Game::getResMgr()->getSound("choose"),
							vector2df(0, SCREEN_HEIGHT+100 ), SCREEN_WIDTH, 40);
	_backItem->Link(_backItem, _backItem);
	items->push_back(_backItem);
	_backItem->LerpTo(0, SCREEN_HEIGHT - 60, 4);
}

void HelpScreen::Update(irr::f32 dt)
{
	Screen::Update(dt);
	if(_backItem->IsPressed() || InputReceiver::IsKeyDown(KEY_ESCAPE))
		changeScreen(new MainMenuScreen(manager));
}
void HelpScreen::Draw()
{
	Screen::Draw();
	Game::getDriver()->draw2DImage(_texture,
		irr::core::vector2d<irr::s32>(0,0),
		irr::core::rect<irr::s32>(0,0, SCREEN_WIDTH, SCREEN_HEIGHT), NULL, SColor(255, 255, 255, 255), true);
	Screen::DrawInterface();
}

HelpScreen::~HelpScreen(void)
{
}
