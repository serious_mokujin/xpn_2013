#pragma once
#include "screen.h"
#include "Helper.h"
#include<irrlicht.h>
#include"GlobalConst.h"
#include"LoadScreen.h"
#include"ExitSplashScreen.h"
#include "AboutScreen.h"
#include"HelpScreen.h"
#include"SettingsScreen.h"
#include"AproveScreen.h"
#include"DifficultySelectScreen.h"
using namespace irr::video;
using namespace irr;
using namespace irr::core;


class MainMenuScreen :
	public Screen
{
private:
	virtual void Draw();
	ITexture* _titleTex;
	IGUIFont* font;
	MenuItem* _startItem;
	MenuItem* _settingsItem;
	MenuItem* _helpItem;
	MenuItem* _aboutItem;
	MenuItem* _exitItem;
public:
	MainMenuScreen(ScreenManager* manager);
	virtual void Load();
	virtual void Unload();
	virtual void Update(irr::f32 dt);
	virtual ~MainMenuScreen(void);
};

