#include "Game.h"

Game* Game::_instance = NULL;
Game::Game(void)
{
	if(_instance)
		throw new std::exception("impossible to create second instance of Game");
	_instance = this;
	scene = NULL;
	device = NULL;
	gui = NULL;
	driver = NULL;
	soundEngine = NULL;
	settings = NULL;
}
void Game::Init(int width, int height)
{
	if(device) return;
	device = createDevice(video::E_DRIVER_TYPE::EDT_OPENGL, core::dimension2d<u32>(width, height),32U, false, false, true, NULL);
	if(!device)
		throw new std::exception("failed to create device");
	InputReceiver::CreateInstance(device);
	
	driver = device->getVideoDriver();
	scene = device->getSceneManager();
	gui = device->getGUIEnvironment();

	soundEngine = createIrrKlangDevice();
	if(!soundEngine)
		throw new std::exception("failed to create sound device");
	
	settings = new Settings();

	rcmgr = new ResourceManager(device, soundEngine);

	AudioManager::CreateInstance(soundEngine, rcmgr);
}
void Game::Run()
{
	Load();
	f32 dt = 0;
	f32 last = 0;
	while (device->run())
	{
		dt = (f32)(device->getTimer()->getTime()-last)/1000.0f;
		
		if(dt>0.5f)
		{
			last = device->getTimer()->getTime();
			continue;
		}
		Update(dt);
		last = device->getTimer()->getTime();
	}
	Unload();
	delete rcmgr;
	delete settings;
	device->drop();
	InputReceiver::DestroyInstance();
	AudioManager::DestroyInstance();
	soundEngine->drop();
}

Game::~Game(void)
{
	
}

IrrlichtDevice* Game::getDevice()
{
	return _instance->device;
}
ISceneManager* Game::getScene()
{
	return _instance->scene;
}
IVideoDriver* Game::getDriver()
{
	return _instance->driver;
}
IGUIEnvironment* Game::getGUI()
{
	return _instance->gui;
}
ResourceManager* Game::getResMgr()
{
	return _instance->rcmgr;
}
ISoundEngine* Game::getSoundEngine()
{
	return _instance->soundEngine;
}
Settings* Game::getSettings()
{
	return _instance->settings;
}
