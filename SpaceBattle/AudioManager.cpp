#include "AudioManager.h"
AudioManager* AudioManager::_instance = NULL;

AudioManager::AudioManager(ISoundEngine* engine, ResourceManager* rcmgr)
{
	this->_musicVol = 1.0f;
	this->_soundVol = 1.0f;
	this->_playMusic = true;
	this->_playSounds = true;
	this->_engine = engine;
	this->_rcmgr = rcmgr;
	_musicPlaying = new std::map<std::string, ISound*>();
}
AudioManager::~AudioManager(void)
{
	for(auto i = _instance->_musicPlaying->begin(); i != _instance->_musicPlaying->end(); ++i)
		if(!i->second->isFinished())
		{
			i->second->stop();
		}
	delete _musicPlaying;
}

void AudioManager::CreateInstance(ISoundEngine* engine, ResourceManager* rcmgr)
{
	_instance = new AudioManager(engine, rcmgr);
}
void AudioManager::DestroyInstance()
{
	delete _instance;
}
void AudioManager::PlayMusic(std::string name, bool looped)
{
	if(!_instance->_playMusic) return;
	if(_instance->_musicPlaying->count(name) > 0)
	{
		if(_instance->_musicPlaying->at(name)->isFinished())
		{
			_instance->_musicPlaying->at(name)->drop();
			(*_instance->_musicPlaying)[name] = _instance->_engine->play2D(_instance->_rcmgr->getSound(name), looped, false, true);
		}
		else if((*_instance->_musicPlaying)[name]->getIsPaused()) 
			(*_instance->_musicPlaying)[name]->setIsPaused(false);
	}
	else
		(*_instance->_musicPlaying)[name] = _instance->_engine->play2D(_instance->_rcmgr->getSound(name), looped, false, true);
}
void AudioManager::PlaySound(ISoundSource* src)
{
	if(!_instance->_playSounds ) return;
	src->setDefaultVolume(_instance->_soundVol);
	_instance->_engine->play2D(src);
}
bool AudioManager::IsPlayingMusic(std::string name)
{
	if(_instance->_musicPlaying->count(name) > 0)
		if(!(*_instance->_musicPlaying)[name]->isFinished())
			return true;
	return false;
}
void AudioManager::StopMusic(std::string name)
{
	if(_instance->_musicPlaying->count(name) > 0)
		if(!(*_instance->_musicPlaying)[name]->isFinished())
			(*_instance->_musicPlaying)[name]->stop();
			
}
void AudioManager::setPauseMusic(std::string name, bool pause)
{
	if(_instance->_musicPlaying->count(name) > 0)
		if(!(*_instance->_musicPlaying)[name]->isFinished())
			(*_instance->_musicPlaying)[name]->setIsPaused(pause);
			
}
void AudioManager::StopMusic()
{
	for(auto i = _instance->_musicPlaying->begin(); i != _instance->_musicPlaying->end(); ++i)
		if(!i->second->isFinished())
			i->second->stop();
}
void AudioManager::setPauseMusic(bool pause)
{
	for(auto i = _instance->_musicPlaying->begin(); i != _instance->_musicPlaying->end(); ++i)
		if(!i->second->isFinished())
			i->second->setIsPaused(pause);
}
void AudioManager::SetMusicVolume(f32 volume)
{
	for(auto i = _instance->_musicPlaying->begin(); i != _instance->_musicPlaying->end(); ++i)
		i->second->setVolume(volume);
	_instance->_musicVol = volume;
}
f32 AudioManager::GetMusicVolume() { return _instance->_musicVol; }
void AudioManager::SetSoundVolume(f32 volume) { _instance->_soundVol = volume; }
f32 AudioManager::GetSoundVolume() { return _instance->_soundVol; }

void AudioManager::setPlayMusic(bool value) { _instance->_playMusic = value; _instance->setPauseMusic(!value); }
void AudioManager::setPlaySounds(bool value) { _instance->_playSounds = value;}

bool AudioManager::isPlayMusic() { return _instance->_playMusic; }
bool AudioManager::isPlaySounds() { return _instance->_playSounds; }