#include "ResourceManager.h"
using namespace std;
using namespace irr::scene;
using namespace irr::video;
using namespace irr::gui;

ResourceManager::ResourceManager(IrrlichtDevice* device, ISoundEngine* soundEngine)
{
	_device = device;
	_soundEngine = soundEngine;
	_meshes = new map<string, IAnimatedMesh*>();
	_textures = new map<string, ITexture*>();
	_fonts = new map<string, IGUIFont*>();
	_soundSources = new map<string, ISoundSource*>();
}

bool ResourceManager::loadTexture(string name, irr::io::path file)
{
	if(_textures->count(name)>0) return true;
	ITexture* t = _device->getVideoDriver()->getTexture(file);
	if(!t)
		return false;
	_textures->insert(pair<string, ITexture*>(name, t));
	return true;
}
bool ResourceManager::loadSound(string name, irr::io::path file, bool preload)
{
	if(_soundSources->count(name)>0) return true;
	ISoundSource* t = _soundEngine->addSoundSourceFromFile(file.c_str(), E_STREAM_MODE::ESM_AUTO_DETECT, preload);
	if(!t)
		return false;
	_soundSources->insert(pair<string, ISoundSource*>(name, t));
	return true;
}
bool ResourceManager::loadMesh(std::string name, irr::io::path file)
{
	if(_meshes->count(name)>0) return true;
	IAnimatedMesh* m = _device->getSceneManager()->getMesh(file);
	if(!m)
		return false;
	_meshes->insert(pair<string, IAnimatedMesh*>(name, m));
	return true;
}
bool ResourceManager::loadFont(std::string name, irr::io::path file)
{
	if(_fonts->count(name)>0) return true;
	IGUIFont* m = _device->getGUIEnvironment()->getFont(file);
	if(!m)
		return false;
	_fonts->insert(pair<string, IGUIFont*>(name, m));
	return true;
}
video::ITexture* ResourceManager::getTexture(string name)
{
	if(_textures->count(name))
		return (*_textures)[name];
	return NULL;
}
scene::IAnimatedMesh* ResourceManager::getMesh(string name)
{
	if(_meshes->count(name))
		return (*_meshes)[name];
	return NULL;
}
IGUIFont* ResourceManager::getFont(string name)
{
	if(_fonts->count(name))
		return (*_fonts)[name];
	return NULL;
}
ISoundSource* ResourceManager::getSound(string name)
{
	if(_soundSources->count(name))
		return (*_soundSources)[name];
	return NULL;
}
void ResourceManager::unloadTexture(std::string name)
{
	_device->getVideoDriver()->removeTexture((*_textures)[name]);
	_textures->erase(name);
}
void ResourceManager::unloadSound(std::string name)
{
	_soundEngine->removeSoundSource(_soundSources->at(name));
	_soundSources->erase(name);
}
void ResourceManager::unloadMesh(std::string name)
{
	_meshes->at(name)->drop();
	_meshes->erase(name);
}
void ResourceManager::unloadFont(std::string name)
{
	_device->getGUIEnvironment()->removeFont(_fonts->at(name));
	_fonts->erase(name);
}

ResourceManager::~ResourceManager(void)
{
	/*
	for (auto i = _soundSources->begin(); i != _soundSources->end(); ++i)
		i->second->drop();
	
	for (auto i = _meshes->begin(); i != _meshes->end(); ++i)
		i->second->drop();
		
	for (auto i = _textures->begin(); i != _textures->end(); ++i)
		i->second->drop();
	for (auto i = _fonts->begin(); i != _fonts->end(); ++i)
		i->second->drop();
		*/
	delete _meshes;
	delete _textures;
	delete _fonts;
	delete _soundSources;
}
