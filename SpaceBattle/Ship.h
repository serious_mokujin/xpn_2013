#pragma once
#include "GameObject.h"
#include<irrlicht.h>
#include"ResourceManager.h"
#include<string>
#include"InputReceiver.h"
#include"IKeyListener.h"
#include "IMouseListener.h"
#include"GlobalConst.h"
#include"Rocket.h"

#include "Affectors.h"
using namespace irr;
using namespace irr::video;
using namespace irr::core;
using namespace irr::scene;


#define LINE_OF_SHIP -130

class Ship :
	public GameObject, IMouseListener
{
private:
	vector3df _position;
	vector3df _rotation;
	f32 _speed;
	bool _rocketSide;
	bool _destroyed;
	bool _win;
	f32 _recoilTime;
	vector3df _destroyDir;
	vector3df _destroyAccel;
	vector3df _destroyRotation;
	Rocket* _rocketManic;
	void updateManic(f32 dt);
	f32 manicAnimation;
	void handleInput(f32 dt);

	bool _moveLeft;
	bool _moveRight;
	f32 getNormalPosition();
	f32 _mouseNormalPosition;
	bool _mouseControll;
	void fire();
	IParticleSystem* _psysteml;
	IParticleSystem* _psystemr;
	IParticleSystem* createFirePS(vector3df pos);
public:
	Ship();
	virtual void Update(f32 dt);
	virtual void OnEvent(EMOUSE_INPUT_EVENT e);
	void Destroy();
	bool IsDestroyed();
	void FlyAway();
	virtual ~Ship(void);
};

