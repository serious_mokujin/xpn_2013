#pragma once
#include "screen.h"
#include<irrlicht.h>
using namespace irr;
using namespace irr::video;


class ExitSplashScreen :
	public Screen
{
private:
	f32 _timeLeft;
public:
	ExitSplashScreen(ScreenManager* manager);
	virtual void Load();
	virtual void Unload(){}
	virtual void Update(irr::f32 dt);
	virtual ~ExitSplashScreen(void);
};
