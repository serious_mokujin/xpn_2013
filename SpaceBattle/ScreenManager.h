#pragma once
#include<list>
#include<irrlicht.h>

using namespace irr;
using namespace irr::core;
using namespace irr::video;

class Screen;
class ScreenManager
{
private:
	Screen* _currentSceen;
	f32 _swapTimer;
	bool _screenChanging;
	ITexture* screenShot; // render target
public:
	ScreenManager();
	void setScreen(Screen* screen);

	void clearScreens();
	void update(f32 dt);
	void draw();
	void DrawscreenShot(f32 opacity);
	virtual ~ScreenManager(void);
};

