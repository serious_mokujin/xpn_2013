
#include <irrlicht.h>
#include"SpaceBattleGame.h"
using namespace irr;
#ifdef _IRR_WINDOWS_
//#pragma comment(lib, "Irrlicht.lib")
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif
int main()
{
	SpaceBattleGame* game = new SpaceBattleGame();
	game->Init(SCREEN_WIDTH, SCREEN_HEIGHT);
	game->Run();
	delete game;
}