#pragma once
#include"ScoreContainer.h"
#include"ScoreAffector.h"

ScoreAffector::ScoreAffector(ScoreContainer* container)
{
	this->container = container;
}
void ScoreAffector::AffectScore(int value)
{
	container->score += value;
}

