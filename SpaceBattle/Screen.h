#pragma once
#include<irrlicht.h>
#include"Game.h"
#include<list>
#include"TextItem.h"
#include "MenuItem.h"
#include"GlobalConst.h"

using namespace irr;
using namespace irr::video;

#define MENU_ITEM_COLOR SColor(255, 255, 255, 255)
#define MENU_HOVER_COLOR SColor(255, 255, 0, 128)
#define MENU_SELECT_COLOR SColor(255, 0, 128, 128)

class ScreenManager;
class Screen
{
	friend class ScreenManager;
private:
	Screen* next;
	bool close;
protected:
	ITexture* backTex;
	std::list<TextItem*>* items;
	virtual void Draw();
	void DrawInterface();
	ScreenManager* manager;
public:
	Screen(ScreenManager* manager);
	virtual void Load(){}
	virtual void Unload(){}
	void changeScreen(Screen* scr);
	virtual void Update(f32 dt);
	virtual ~Screen(void);
};

