#include "Explosible.h"


void Explosible::addExplosion(const vector3df& pos, const u32& scale, ISceneNode* parent)
{
	ISceneManager* scene = Game::getScene();
    IParticleSystem* system = createParticleSystem(parent, scene, 6);
    system->setPosition(pos);
    system->setScale(vector3df(scale));
    system->updateAbsolutePosition();

    ///Spark Trails
    particle::IParticleDrawer* drawer = system->addParticleDrawer();

	for (int j = 0; j < 2; j++)
	{	
		for (int i = 0; i < 8; i++)
		{
			drawer->addUVCoords(particle::SParticleUV(vector2df( i * 0.125f, j * 0.125f), 
											vector2df((i*0.125f)+0.125f, j*0.125f), 
											vector2df(i*0.125f,(j*0.125f) + 0.125f),
											vector2df((i*0.125f)+0.125f,(j*0.125f) + 0.125f)));
		}
	}
    particle::IParticleEmitter* emitter = drawer->addStandardEmitter(vector3df(0,0,0), 
																		vector3df(0.1,0,0), 
																		vector3df(3,0,0), 
																		vector3di(0, 180, 0), 100, 200, 50, 1000, 1200,
																		vector2df(1,1), 
																		vector2df(2,2), 
																		vector2df(2), SColor(255,200,180,0), SColor(255,0,0,0));

    particle::IParticleAffector* affector = new ColorAffectorQ(irr::video::SColor(255, 100, 0, 0), irr::video::SColor(0,0,0,0));
    drawer->addAffector(affector);
    affector->drop();
    //affector = new GravityAffector(irr::core::vector3df(0.f,10.0f,0.f));
    //drawer->addAffector(affector);
    //affector->drop();

	///Smoke Trails
    drawer = system->addTrailParticleDrawer();
	for (int j = 2; j < 4; j++)
	{	
		for (int i = 0; i < 8; i++)
		{
			particle::SParticleUV uv = particle::SParticleUV(vector2df( i * 0.125f, j * 0.125f), 
											vector2df((i*0.125f)+0.125f, j*0.125f), 
											vector2df(i*0.125f,(j*0.125f) + 0.125f),
											vector2df((i*0.125f)+0.125f,(j*0.125f) + 0.125f));
			uv.turnRight();
			drawer->addUVCoords(uv);
		}
	}
    emitter = drawer->addStandardEmitter(vector3df(0,0,0), vector3df(0,0,0), vector3df(0,10,0), vector3di(180, 0, 180), 0, 500, 10, 400, 600, vector2df(0.25,1.0), vector2df(0.25,1.0), vector2df(2.f), SColor(255,255,128,50), SColor(255,255,128,50));

    affector = new ColorAffectorQ(SColor(128,128,128,50), SColor(0,0,0,0));
    drawer->addAffector(affector);
    affector->drop();


    SMaterial overrideMaterial;
    overrideMaterial.MaterialType = EMT_TRANSPARENT_ADD_COLOR;
    overrideMaterial.setTexture(0, Game::getResMgr()->getTexture("particles"));
    overrideMaterial.setFlag(EMF_LIGHTING, false);
    overrideMaterial.setFlag(EMF_ZWRITE_ENABLE, true);
    overrideMaterial.setFlag(EMF_BACK_FACE_CULLING, false);
    overrideMaterial.MaterialTypeParam = pack_textureBlendFunc (EBF_SRC_ALPHA, EBF_ONE_MINUS_SRC_ALPHA, EMFN_MODULATE_1X, EAS_VERTEX_COLOR | EAS_TEXTURE );
    system->setOverrideMaterial(overrideMaterial);
	
    irr::scene::ISceneNodeAnimator* anim = scene->createDeleteAnimator(3000);
    system->addAnimator(anim);
    anim->drop();
	system->drop();
}
