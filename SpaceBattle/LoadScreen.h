#pragma once
#include<irrlicht.h>
#include"Screen.h"
using namespace irr;
using namespace irr::video;
class LoadScreen:public Screen
{

private:
	f32 _time;
	bool _loaded;
public:
	LoadScreen(ScreenManager* manager);
	virtual void Load();
	virtual void Unload();
	virtual void Update(irr::f32 dt);
	~LoadScreen(void);
};

