#pragma once
#include<map>
#include<string>
#include<irrlicht.h>
#include<irrKlang.h>
#include"ResourceManager.h"

using namespace irr;
using namespace irr::core;
using namespace irrklang;

class AudioManager
{
private:
	ISoundEngine* _engine;
	ResourceManager* _rcmgr;
	f32 _musicVol;
	f32 _soundVol;
	bool _playMusic;
	bool _playSounds;
	std::map<std::string, ISound*>* _musicPlaying;
	static AudioManager* _instance;

	AudioManager(ISoundEngine* engine, ResourceManager* rcmgr);
	~AudioManager(void);
public:
	
	static void CreateInstance(ISoundEngine* engine, ResourceManager* rcmgr);
	static void DestroyInstance();
	static void PlayMusic(std::string name, bool looped = false);
	static void PlaySound(ISoundSource* src);
	static bool IsPlayingMusic(std::string name);
	static void StopMusic(std::string name);
	static void setPauseMusic(std::string name, bool pause);
	static void StopMusic();
	static void setPauseMusic(bool pause);
	static void SetMusicVolume(f32 volume);
	static f32 GetMusicVolume();
	static void SetSoundVolume(f32 volume);
	static f32 GetSoundVolume();

	static void setPlayMusic(bool value);
	static void setPlaySounds(bool value);

	static bool isPlayMusic();
	static bool isPlaySounds();
};

