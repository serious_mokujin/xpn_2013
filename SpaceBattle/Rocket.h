#pragma once
#include "GameObject.h"
#include<irrlicht.h>
#include<list>
#include "Affectors.h"
using namespace irr;
using namespace irr::video;
using namespace irr::core;
using namespace irr::scene;
class Rocket :
	public GameObject
{
private:
	bool _active;
	f32 _speed;
	f32 _accel;
	ISoundSource* _fireSound;
	static std::list<Rocket*> pool;
	void Launch(vector3df pos, bool directUp);

	void addFirePS(vector3df pos);
public:
	Rocket();
	virtual void Update(f32 dt);
	void Frezee();
	static void LaunchRocket(vector3df pos, bool directUp);
	static void UpdatePool(f32 dt);
	static void ClearPool();
	static Rocket* CheckCollisions(GameObject* o)
	{
		for(std::list<Rocket*>::iterator i = pool.begin(); i != pool.end(); ++i)
			if((*i)->_active)
				if((*i)->GetHitbox().intersectsWithBox(o->GetHitbox()))
						return (*i);
		return NULL;
	}
	virtual ~Rocket(void);
};

