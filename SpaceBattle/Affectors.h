#pragma once
#include<irrlicht.h>
#include "ParticleEngine/IParticleSystem.h"
#include "ParticleEngine/IParticleDrawer.h"
#include "ParticleEngine/IEmitterRegion.h"
#include "ParticleEngine/IParticleEmitter.h"
#include "ParticleEngine/Particle.h"

using namespace irr;
using namespace irr::video;
using namespace irr::core;


class GravityAffector : public irr::scene::particle::IParticleAffector
{
public:
    GravityAffector(const irr::core::vector3df& gravity)
    {
        Gravity = gravity;
    }
    void affect(irr::scene::particle::Particle* particle, irr::u32 timeMs, irr::f32 diff)
    {
        particle->Speed += Gravity*diff;
    }
    irr::core::vector3df Gravity;

};

class ColorAffectorQ : public particle::IParticleAffector
{
public:
    ColorAffectorQ(const SColor& targetColor0, const SColor& targetColor1)
    {
        Target0 = targetColor0;
        Target1 = targetColor1;
    }
    void affect(particle::Particle* particle, u32 timeMs, f32 diff)
    {
        f32 lifeTime = particle->LifeTime;//particle->DestroyTimeMs-particle->CreateTimeMs;
        f32 livedTime = particle->TimeLived;//timeMs-particle->CreateTimeMs;


        particle->Color = particle->StartColor.getInterpolated_quadratic(Target0, Target1, livedTime/lifeTime);
    }
    SColor Target0;
    SColor Target1;

};
