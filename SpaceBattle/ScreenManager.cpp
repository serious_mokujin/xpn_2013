#include "ScreenManager.h"
#include "Screen.h"

ScreenManager::ScreenManager()
{
	_currentSceen = NULL;
	_screenChanging = false;
	_swapTimer = 1.0f;
	this->screenShot = Game::getDriver()->addRenderTargetTexture(irr::core::dimension2d<irr::u32>(SCREEN_WIDTH, SCREEN_HEIGHT));
}
void ScreenManager::clearScreens()
{
	if(_currentSceen)
	{
		_currentSceen->Unload();
		delete _currentSceen;
	}
}
void ScreenManager::draw()
{
	if(_screenChanging && _swapTimer >= 0)
	{
		this->_currentSceen->Draw();
		this->DrawscreenShot(_swapTimer);
	}
	else
	{
		this->_currentSceen->Draw();	
	}
}
void ScreenManager::update(irr::f32 dt)
{
	if(_screenChanging)
	{
		if(_swapTimer > 0) 
			_swapTimer -= dt;
		else
		{
			_swapTimer = 1;
			_screenChanging = false;
		
			if(_swapTimer < 0)
				_swapTimer = 0;
		}
	}
	_currentSceen->Update(dt);
	if(_currentSceen->close)
		setScreen(_currentSceen->next);
}
void ScreenManager::setScreen(Screen* screen)
{
	if(this->_currentSceen)
	{
		Game::getDriver()->setRenderTarget(screenShot);	
		_currentSceen->Draw();
		Game::getDriver()->setRenderTarget(0, true, true, 0);
		this->_currentSceen->Unload();
		delete _currentSceen;
	}
	this->_currentSceen = screen;
	if(this->_currentSceen)
		_currentSceen->Load();
	_swapTimer = 1;
	_screenChanging = true;
}
void ScreenManager::DrawscreenShot(f32 opacity)
{
	u32 a = (u32)(255.0f * opacity);
	if(a > 250)
	{
		int i = 0;
		i = 5;
	}
	SColor c[4] ={ SColor(a, 255, 255, 255), SColor(a, 255, 255, 255), SColor(a, 255, 255, 255), SColor(a, 255, 255, 255)};
	Game::getDriver()->draw2DImage(screenShot, recti(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT), recti(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT), NULL, c, false);
}
ScreenManager::~ScreenManager(void)
{
	Game::getDriver()->removeTexture(screenShot);
}
