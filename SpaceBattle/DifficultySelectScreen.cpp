#include "DifficultySelectScreen.h"


DifficultySelectScreen::DifficultySelectScreen(ScreenManager* manager) : Screen(manager)
{
}
void DifficultySelectScreen::Load()
{
	this->backTex = Game::getResMgr()->getTexture("background");
	
	font = Game::getResMgr()->getFont("space");
	
	ISoundSource* s1 = Game::getResMgr()->getSound("select");
	ISoundSource* s2 = Game::getResMgr()->getSound("choose");
	_titleItem = new TextItem("SETTINGS", Game::getResMgr()->getFont("spaceBig"), SColor(255, 255, 215, 0), vector2df(0, -2000),SCREEN_WIDTH, 60); 
	_easyItem = new MenuItem("easy", font, MENU_ITEM_COLOR, MENU_HOVER_COLOR, MENU_SELECT_COLOR, s1, s2,vector2df(0, -300), SCREEN_WIDTH, 60); 
	_middleItem = new MenuItem("middle", font, MENU_ITEM_COLOR, MENU_HOVER_COLOR, MENU_SELECT_COLOR, s1, s2,vector2df(0, -360), SCREEN_WIDTH, 60);
	_hardItem = new MenuItem("hard", font, MENU_ITEM_COLOR, MENU_HOVER_COLOR, MENU_SELECT_COLOR, s1, s2,vector2df(0, -420), SCREEN_WIDTH, 60);
	_crazyItem = new MenuItem("insane", font, MENU_ITEM_COLOR, MENU_HOVER_COLOR ,MENU_SELECT_COLOR, s1, s2,vector2df(0, -480), SCREEN_WIDTH, 60);
	_backItem =  new MenuItem("back", font, MENU_ITEM_COLOR, MENU_HOVER_COLOR ,MENU_SELECT_COLOR, s1, s2, vector2df(0, SCREEN_HEIGHT+100), SCREEN_WIDTH, 40);
	items->push_back(_titleItem);
	items->push_back(_easyItem);
	items->push_back(_middleItem);
	items->push_back(_hardItem);
	items->push_back(_crazyItem);
	items->push_back(_backItem);
	_easyItem->Link(_backItem, _middleItem);
	_middleItem->Link(_easyItem, _hardItem);
	_hardItem->Link(_middleItem, _crazyItem);
	_crazyItem->Link(_hardItem, _backItem);
	_backItem->Link(_crazyItem, _easyItem);

	_titleItem->LerpTo(0, 100, 3);
	_easyItem->LerpTo(0, 300, 3);
	_middleItem->LerpTo(0, 360, 3);
	_hardItem->LerpTo(0, 420, 3);
	_crazyItem->LerpTo(0, 480, 3);
	_backItem->LerpTo(0, SCREEN_HEIGHT - 60, 4);
}
void DifficultySelectScreen::Unload(){}

void DifficultySelectScreen::Update(irr::f32 dt)
{
	Screen::Update(dt);
	if(_easyItem->IsPressed())
		Game::getSettings()->difficulty = DIFFICULTY_LEVEL::EASY;
	if(_middleItem->IsPressed())
		Game::getSettings()->difficulty = DIFFICULTY_LEVEL::MEDIUM;
	if(_hardItem->IsPressed())
		Game::getSettings()->difficulty = DIFFICULTY_LEVEL::HARD;
	if(_crazyItem->IsPressed())
		Game::getSettings()->difficulty = DIFFICULTY_LEVEL::CRAZY;
	if(_easyItem->IsPressed() || _middleItem->IsPressed() || _hardItem->IsPressed() || _crazyItem->IsPressed())
		changeScreen(new LoadScreen(manager));

	if(_backItem->IsPressed() || InputReceiver::IsKeyDown(KEY_ESCAPE))
		changeScreen(new MainMenuScreen(manager));
	
}
void DifficultySelectScreen::Draw()
{
	Screen::Draw();
	Screen::DrawInterface();
}

DifficultySelectScreen::~DifficultySelectScreen(void)
{
}
