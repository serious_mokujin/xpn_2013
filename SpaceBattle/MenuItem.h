#pragma once
#include<string>
#include<irrlicht.h>
#include"Game.h"
#include"TextItem.h"
#include"IMouseListener.h"
#include"IKeyListener.h"

using namespace std;
using namespace irr;
using namespace irr::gui;
using namespace irr::core;
using namespace irr::video;

class MenuItem : public TextItem, IMouseListener, IKeyListener
{
private:
	MenuItem* _next;
	MenuItem* _prev;
	SColor _selectedColor;
	SColor _chooseColor;
	bool _selected;
	ISoundSource* _selectSound;
	ISoundSource* _chooseSound;
	bool _pressed;
	bool _blockInput;
	bool _selectLock; // ����������� ���������. ����� ��� ����, ����� �������� ���� ����� ���� �������� �� ����. ���� ���������� �����, �� ��������� �� ��������� � ���������� �������� ����
public:
	MenuItem(stringw text, 
		IGUIFont* font, 
		SColor color,
		SColor selectColor, 
		SColor chooseColor, 
		ISoundSource* selectSound, 
		ISoundSource* chooseSound,
		vector2df pos, int width, int height);
	void Link(MenuItem* prev, MenuItem* next);
	MenuItem* getNext();
	MenuItem* getPrev();

	SColor getSelectColor();
	void setSelectColor(SColor color);
	SColor getChooseColor();
	void setChooseColor(SColor color);
	void setChooseSound(ISoundSource* sound);
	void setSelectSound(ISoundSource* sound);
	bool IsSelected();
	bool IsPressed();
	void Select();
	void Unselect();
	void Press();
	void Release();
	virtual void Update(f32 dt);
	void UpdateRecursive(f32 dt);
	void DrawRecursive();
	void DeleteRecursive();
	void BlockInputRecursive();
	void UnblockInputRecursive();
	virtual void Draw();
	virtual void OnPress(EKEY_CODE);
	virtual void OnRelease(EKEY_CODE);
	virtual void OnEvent(EMOUSE_INPUT_EVENT);
	virtual ~MenuItem(void);
};

