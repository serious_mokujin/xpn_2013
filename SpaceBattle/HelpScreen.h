#pragma once
#include "screen.h"
#include "Helper.h"
#include "MainMenuScreen.h"


class HelpScreen :
	public Screen
{
private:
	ITexture* _texture;
	virtual void Draw();
	MenuItem* _backItem;
public:
	HelpScreen(ScreenManager* manager);
	~HelpScreen(void);
	virtual void Load();
	virtual void Update(irr::f32 dt);
};

